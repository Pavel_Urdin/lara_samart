<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Model\Catalog;

//use App\Model\products_catalog';
//use App\Model\products_cat;



class CartController extends Controller
{

  public function save(Request $request){


	$catalog = new Catalog;

	$catalog->name = $request['name'];
	$catalog->sections = json_encode($request['sections']);

	$catalog->save();

	return $catalog;



	/*
      $id = $catalog->id;
      $products = $request['prod'];
      try{

      foreach($products as $k=>$v){

      $productsModel = new products_cats;
      $productsModel->catalog_id = $id;
      $productsModel->product_id = $v;
      $productsModel->save();

      }

      }
      catch(\Exception $e){
      echo $e->getMessage();
      }

    */


  }


  public function edit(Request $request){


  }


  public function destroy(Request $id){

  }




  function showOrders(Request $request){

	return $request->id;
    //Catalog::all('id','name');

  }



  public function show(Request $request){

    $id = $request->id;

    //$catalog = Catalog::get()->where('id','=',$id);
    //$catalog = Catalog::get($id);

    $catalog = DB::table('catalogs')->where('id','=',$id)->get();
    return $catalog;

  }


}




/*
  Route::any('/api/catalog'/save', 'Api\CatalogController@save')->name('api.admin.category.save');
  Route::any('/api/catalog'/edit', 'Api\CatalogController@edit')->name('api.admin.category.edit');
  Route::any('/api/catalog'/delete', 'Api\CatalogController@')->name('api.admin.category.delete');
*/
