<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Model\Category;

//use App\Model\products_category;
//use App\Model\products_cat;

use App\products_cats;


class CategoryController extends Controller
{


  public function save(Request $request){


	$category = new Category;

	$category->name = $request['name'];
	$category->url = $request['url'];
	$category->discount = $request['discount'];

    $category->save();

	$id = $category->id;
    $products = $request['prod'];


    try{

      foreach($products as $k=>$v){

        $productsModel = new products_cats;
        $productsModel->category_id = $id;
        $productsModel->product_id = $v;
        $productsModel->save();

      }

    }
    catch(\Exception $e){
      echo $e->getMessage();
    }


    /*
      if($products){


      //$drop = new products_category;
      //$drop->where('products_category.category_id','=',$id)->delete();

      $i=1;

      foreach($products as $v){

      $productsModel = new products_category;

      $productsModel->category_id =1;
      $productsModel->product_id = 2;
      $productsModel->save();


      }


      };
    */

    return $products;

  }

  public function edit(Request $request){


    $id = $request->id;
    $category = Category::find($id);

    $category->name = $request['name'];
    $category->url = $request['url'];
    $category->discount = $request['discount'];
    $category->save();

    $products = $request['prod'];

    $drop = new products_cats;
    $drop->where('products_cats.category_id','=',$id)->delete();



    foreach($products as $k=>$v){
      $productsModel = new products_cats;
      $productsModel->category_id = $id;
      $productsModel->product_id = $v;
      $productsModel->save();
    }


    return $category;

  }


  public function destroy(Request $id){

	Category::destroy($id[0]);
    return $id;

  }


  public function show(Request $request){

    return Category::all('id','name','discount');

  }


  public function showCategory(Request $request){


    //$category = Category::find($request->id)->join('products_cats', 'category.id', '=', 'products_cats.category_id')->get();

    /*
      $category = DB::table('categories')->join('products_cats', 'categories.id', '=', 'products_cats.category_id')
      ->where('categories.id', '=', $request->id)
      ->select('categories.name','categories.url','products_cats.product_id')
      ->get();
    */
    //$request->id = 11;


    $category = Category::find($request->id);

    $prod =DB::table('products_cats')->where('category_id', '=', $request->id)
          ->select('products_cats.product_id')
          ->get();

    $p=[];
    forEach($prod as $k=>$v){
      array_push($p, $v->product_id);
    }

    $category['prod'] = $p;

    return $category;

    //return Category::find($request->id)->leftJoin('products_cat', $request->id, '=', 'products_cat.category_id')->get();
	//return Category::find($request->id);

  }

}




/*
  Route::any('/api/category/save', 'Api\CategoryController@save')->name('api.admin.category.save');
  Route::any('/api/category/edit', 'Api\CategoryController@edit')->name('api.admin.category.edit');
  Route::any('/api/category/delete', 'Api\CategoryController@')->name('api.admin.category.delete');
*/
