<?php

namespace App\Http\Controllers\Api;

use App\Model\Product;
use App\components;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */



  public function index(Request $request)
  {

	$data = request()[0];
	$images = request()[1];
	$description = json_encode(request()[2]);
	$imgDefault = request()[3];
	$components = request()[4];

	$product = new Product;

	$product->art = $data['art'];
	$product->name = $data['name'];
	$product->brand = $data['brand'];
	$product->price = $data['price'];
	$product->special_price = $data['special_price'];
	$product->url_key = $data['url_key'];
	$product->description = $description;

	//$product->image=$imgDefault;

	$product->save();


	$id = $product->id;
	$product = Product::find($id);
	$defDir = $this->saveProductImage($images,$id);
	if($imgDefault) $product->image=$defDir.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR.$imgDefault;
	$product->save();

    if($components){

      $drop = new components;

      $drop->where('components.product_id','=',$id)->delete();
      
      foreach($components as $k=>$v){
        $componentsModel = new components;
        $componentsModel->product_id = $product->id;
		$componentsModel->component_id=(int) $k;
		$componentsModel->qty=(int) $v;
        $componentsModel->save();
      }
    };



	//$Product::create(['art'=>$data['sku']]);


	//$image = request()->toArray();
	//request()->image->move(public_path('images'), $imageName);

	/*
      request()->validate([
      '0'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
    */

	/*
      $imageName = time().'.'.request()->data->getClientOriginalExtension();
      request()->image->move(public_path('images'), $imageName);
    */

	return $product->image;

  }

  private function saveProductData($productData){

  }



  private function saveProductImage($images,$id){

	$defImage = NULL;

	$catalogName ="images".DIRECTORY_SEPARATOR."products".DIRECTORY_SEPARATOR.date('Y-m');

	is_dir($catalogName) == true ? true : mkdir($catalogName);
	$dirName = $catalogName.DIRECTORY_SEPARATOR.$id;

	is_dir($dirName) == true ? true : mkdir($dirName);
	array_map('unlink', glob($dirName.DIRECTORY_SEPARATOR.'*'));

	if($images){

      forEach($images as $image){
		//$imageName = time().str_replace(" ","",$image['name']);
		$imageName = $image['name'];
		$imageData = $image['data'];
		$addres = $dirName.DIRECTORY_SEPARATOR.$imageName;
		if(!$defImage) $defImage = $addres;
		$data = substr($imageData,strlen('data:image/png;base64,'),strlen($imageData));
		file_put_contents($addres,base64_decode($data));

      }
	}

	return $catalogName;

  }




  private function saveProductDescription($productDescription){

  }














  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function show(Request $request)
  {


    //return  __DIR__;


	return Product::all('id','art','name','brand','special_price','price');

  }



  public function showProduct(Request $request)
  {

	return Product::find($request->id);

  }

  public function imageDefault(Request $request){

	$data = Product::find($request->id);
	$imgDefalt = $data->image;
	$imgDefalt = last(explode('/',$imgDefalt));
	return $imgDefalt;

  }


  public function images(Request $request){

	$data = Product::find($request->id);
	$imgDefalt = $data->image;

	if($imgDefalt) $imgs = $this->getImages($imgDefalt);

	$img = [];

	foreach($imgs as $key=>$val){
      $imgData = 'data:image/png;base64,'.base64_encode(file_get_contents($val));
      $imgName = last(explode('/',$val));
      $img[$imgName]= 'data:image/jpg;base64,'.base64_encode(file_get_contents($val));
	}

	return $img;

  }


  private function getImages($imgDefaltAddress){

	if($imgDefaltAddress){
      $imgAddress = explode(DIRECTORY_SEPARATOR,$imgDefaltAddress);
      array_pop($imgAddress);
      $imgAddress = implode(DIRECTORY_SEPARATOR,$imgAddress);

      $getImgAddress = function($img) use ($imgAddress){
		return $imgAddress.DIRECTORY_SEPARATOR.$img;
      };

      if(is_dir($imgAddress)){
		$images = scandir($imgAddress);
		$images = array_map($getImgAddress,array_diff(($images),array('..', '.')));
		return $images;
      }else{
		return false;
      }
	}else{
      return false;
	}
  }




  public function edit(Request $request)
  {





	$data = request()[0];
	$images = request()[1];
	$description = json_encode(request()[2]);
	$imgDefault = request()[3];
	$components = request()[4];

	$id = (int) $data['id'];
	$product = Product::find($id);


	$product->art = $data['art'];
	$product->name = $data['name'];
	$product->brand = $data['brand'];
	$product->price = $data['price'];
	$product->special_price = $data['special_price'];
	$product->url_key = $data['url_key'];
	$product->description = $description;

	$defDir = $this->saveProductImage($images,$id);
	if($imgDefault) $product->image=$defDir.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR.$imgDefault;
	$product->save();


    if($components){

      $drop = new components;

      $drop->where('components.product_id','=',$id)->delete();
      
      foreach($components as $k=>$v){
        $componentsModel = new components;
        $componentsModel->product_id = $product->id;
		$componentsModel->component_id=(int) $k;
		$componentsModel->qty=(int) $v;
        $componentsModel->save();
      }
    }else{

      $drop = new components;
      $drop->where('components.product_id','=',$id)->delete();
    };









	//$Product::create(['art'=>$data['sku']]);
	//$image = request()->toArray();
	//request()->image->move(public_path('images'), $imageName);

	/*
      request()->validate([
      '0'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
    */

	/*
      $imageName = time().'.'.request()->data->getClientOriginalExtension();
      request()->image->move(public_path('images'), $imageName);
    */

	return $id;


  }




  public function  getComponents($id){

    /*
      $data = request()[0];
      $images = request()[1];
      $description = json_encode(request()[2]);
      $imgDefault = request()[3];
      $components = request()[4];
      $id = (int) $data['id'];
    */

    //$id = (int) $id;
    //dd($id);

    //dump($id);

    //$components = components::where('parent_id',$id)->get();


    /*
      $product = Product::find($id);
      $components = components::select("component_id","qty")
      ->where('parent_id',$id)->get();

      $product["components"]=$components;
    */


    $components = DB::table('components')-> where('components.product_id','=',$id);



    $product = DB::table('products')
             ->leftJoinSub($components, 'components', function($join){
               $join->on('products.id','=','components.component_id');
             })
             ->where('products.id','<>',$id)
             ->select('products.*', 'components.qty')
             ->get();



    /*
      $product = DB::table('products')
      ->join('components', function($join) use ($id){

      $join->on('products.id','=','components.component_id')
      ->where('components.parent_id','=',$id);
      })
      ->select('products.*', 'components.qty')
      ->get();
    */

    /*
      $com = DB::table('components')
      ->where('parent_id', '=', 100)
      ->get();
    */

    return $product;

  }







  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
	$id = $request[0];
	//$product = Product::find($id);
	Product::destroy($id);
  }
}
