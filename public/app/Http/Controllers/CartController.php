<?php

namespace App\Http\Controllers;

use App\Model\Product;

use App\orders;
use App\order_contents;

//use App\Model\Order\Order;
//use App\Model\Order\Orders;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CartController extends Controller
{



    public function __construct()
    {

    }




    public function index(Request $request)

    {

      //String


      $cart = $request->session()->get('cart');
      
      //dump($cart);
      //dump($request->session());


      /* 
      $arrProd = [];
      if($cart){
      $arrCart = explode(";",$cart);

        foreach($arrCart as $k=>$v){

          $temp = explode("&",$v);

          //art
          $temp[0] = str_replace("art=","",$temp[0]);
          //qty
          $temp[1] = str_replace("qty=","",$temp[1]);
          array_push($arrProd,$temp);

        }


        foreach($arrProd as $key=>$prod){
          
          $art = $prod[0];
          $arrProd[$key][0] = DB::table('products')->where('art', $art)->get()->first();

        }

      }
      return view('cart/cart',['product'=>$arrProd]);
      */



      $products = [];


      if($cart){
        foreach($cart as $key=>$qty){
          if($key<>'cartid'){
            $products[$key] = DB::table('products')->where('art', $key)->get()->first();
            $products[$key]->qty = (int)$qty;
          }
        }
      }


      return view('cart/cart',['products'=>$products]);


      //return view('cart/cart');


    }


    public function addToCart(Request $request){


      $cart = $request->input('cart');   

      $request->session()->forget('cart');
      $request->session()->put('cart', $cart);

      return  $cart;

    }


    public function changeCart(Request $request){


      $cart = $request->input('cart');   

      $request->session()->forget('cart');
      $request->session()->put('cart', $cart);

      return  $cart;

    }



    public function delCart(Request $request){

      $request->session()->forget('cart');      
      
    }















   public function final(Request $request){



     $cart = $request->get('cart');


     //Save cart

     //-------------------------------------------------//

     $order = new orders;

     $order->cart_id = $request->get('cartId');
     $order->name = $request->get('name');
     $order->tel = $request->get('tel');
     $order->email = $request->get('email');
     $order->address = $request->get('address');
     $order->description = $request->get('description');

     $order->save();





     //test

     /* 
     $order->cart_id = "9260047";
     $order->name = "Рамка 1 пост";
     $order->tel = "123123";
     $order->email = "admin@admin.com";
     $order->address = "qweqwe";
     $order->description = "asdsadsa";
     */

     //------------------------------------------------//





     foreach($cart as $k=>$v){
   
       if($k<>'cartid'){


         $order_contents = new order_contents;
         $product = new Product;

         $p = $product->where('products.art','=',$k)->get();
         $product = $p[0];

         $order_contents->cart_id = $request->get('cartId');
         $order_contents->art = $k; 
         $order_contents->brand = $product->brand; 
         $order_contents->name = $product->name; 
         $order_contents->special_price = $product->special_price; 
         $order_contents->price = $product->price; 
         $order_contents->qty = $v;

         $order_contents->save();

       }

     }


     
     return $cart; 




     //return view('cart/final');





     //$product = DB::table('product')-> where('product.art','=','770011');
     /*
     $order_contents = new order_contents;

     $order_contents->cart_id = $request->get('cartId');

     $order_contents->art = "qweqweqwe"; 
     $order_contents->brand = "qweqweqwe"; 
     $order_contents->name = "qweqweqwe"; 

     $order_contents->special_price = 100.00; 
     $order_contents->price = 1000.00; 
     $order_contents->qty = 10.00; 

     $order_contents->save();
     */


     //return view('cart/final');


   }






   public function lastPage(){
     return view('cart/final');
   }  
   
  
}
