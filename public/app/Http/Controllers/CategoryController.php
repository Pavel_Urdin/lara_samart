<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Category;
use App\Model\Product;
use App\Model\products_cats;

use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */


  public function __construct()
  {
    //$this->middleware('auth');
  }


  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */

  public function index($id)
  {

    $data = Category::find($id);
    $prod = DB::table('products_cats')->join('products','products_cats.product_id','=','products.id')
          ->where('products_cats.category_id','=',$id)
          ->select('products.id','products.art','products.name','products.special_price','products.price', 'products.image')
          ->get();


    //return view('category',array('all' => $data,'description'=>$description,'imgDefalt'=>$data->image,'images'=>$images,'components'=>$subproduct));

    return view('category',array('products' => $prod));

  }
}
