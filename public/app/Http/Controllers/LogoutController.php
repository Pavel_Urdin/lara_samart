<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;

use Illuminate\Routing\Redirector;



class LogoutController extends Controller
{
  
  public function index(Request $request){

    $request->session()->flush();
    return redirect()->route('home');

  }
}
