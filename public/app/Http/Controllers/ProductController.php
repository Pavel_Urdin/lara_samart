<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Product;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */


  public function __construct()
  {
    //$this->middleware('auth');
  }


  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index(Request $request,$id)
  {


    //dump($request->session());


    $data = Product::find($id);
    $description = json_decode($data['description']);
    $imgDefalt = $data->image;
    
    if($imgDefalt){

      $imgAddress = explode(DIRECTORY_SEPARATOR,$imgDefalt);
      $imgDefName = array_pop($imgAddress);
      $imgAddress = implode(DIRECTORY_SEPARATOR,$imgAddress);

      $getImgAddress = function($img) use ($imgAddress){
        return $imgAddress.DIRECTORY_SEPARATOR.$img;
      };

      if(is_dir($imgAddress)){

        $images = scandir($imgAddress);
        $images = array_diff(($images),array('..', '.',$imgDefName));
        array_unshift($images, $imgDefName);

        $images = array_map($getImgAddress,$images);

      }

    }else{
      $imgDefalt=null;
      $images = null;
    }




    $components = DB::table('components')-> where('components.product_id','=',$id);

    $subproduct = DB::table('products')
             ->JoinSub($components, 'components', function($join){
               $join->on('products.id','=','components.component_id');
             })
             ->where('products.id','<>',$id)
             ->select('products.*', 'components.qty')
             ->get();

    //return dump($subproduct[0]);

    return view('product',array('all' => $data,'description'=>$description,'imgDefalt'=>$data->image,'images'=>$images,'components'=>$subproduct));

  }
}
