<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;


class SearchController extends Controller
{
  
  public function index(Request $request){


    $request->session()->flush();


    $param= $request->search;


    $prod = DB::table('products')->where('name', 'like', '%'.$param.'%')->get();
    //$prod = DB::select('select * from products where name like %param%', [1]);

    return view('category',array('products' => $prod));


  }

}
