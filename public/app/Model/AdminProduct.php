<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminProduct extends Model
{
  protected $fillable = ['sku','brand','name', 'price', 'special_price', 'url_key', 'image', 'description'];
}
