<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  
  public $timestamps = false;
    
  protected $fillable = ['art','brand','name', 'price', 'special_price', 'url_key', 'image', 'description','status'];

}
