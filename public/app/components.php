<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class components extends Model
{
  public $timestamps = false;
  protected $fillable = ['parent_id','component_id','qty'];
}
