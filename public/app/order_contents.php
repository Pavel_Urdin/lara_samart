<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order_contents extends Model
{
  
  public $timestamps = false;
  
  public $fillable = ['id','order_id','art','brand','name','special_price','price','qty','created_at','updated_at'];

}
