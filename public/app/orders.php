<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orders extends Model
{

  public $timestamps = false;

  public $fillable = ['id','cart_id','name','tel','email','address','description','created_at','updated_at'];

}
