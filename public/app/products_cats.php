<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products_cats extends Model
{

     public $timestamps = false;
     public $fillable = ['id','category_id','product_id'];
}
