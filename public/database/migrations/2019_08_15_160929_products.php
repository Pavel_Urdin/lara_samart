<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('art',255);
            $table->string('brand',255);
            $table->string('name',255);
            $table->decimal('price', 10, 2)->unsigned();            
            $table->decimal('special_price', 10, 2)->unsigned();
            $table->string('url_key')->nullable(false);
            $table->string('image',255)->nullable(true);
            $table->json('description')->nullable(true);
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->unique(['art', 'brand']); 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
