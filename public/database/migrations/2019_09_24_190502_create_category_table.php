<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {


            $table->engine = 'InnoDB';

            $table->bigIncrements('id');

            $table->string('name')->nullable(false);
            $table->string('url')->nullable(false);
            $table->float('discount')->nullable(false);

            $table->text('description')->nullable(true);
            $table->string('title')->nullable(true);

            $table->string('meta_title')->nullable(true);
            $table->text('meta_keywords')->nullable(true);
            $table->text('meta_description')->nullable(true);

            $table->unique('url'); 
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
