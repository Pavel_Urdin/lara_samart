<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()
    {

        Schema::create('orders', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('cart_id',255);

            $table->string('name',255)->nullable(true);
            $table->string('tel',255)->nullable(true);

            $table->string('email',255)->nullable(true);
            $table->string('address',255)->nullable(true);

            $table->text('description')->nullable(true);

            /*
            $table->decimal('total_special_price', 10, 2)->unsigned();
            $table->decimal('total_price', 10, 2)->unsigned();            
            */

            $table->timestamps();
            $table->unique('cart_id'); 


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('orders');

    }

}
