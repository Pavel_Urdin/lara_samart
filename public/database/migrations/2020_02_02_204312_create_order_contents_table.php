<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderContentsTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()
    {

        Schema::create('order_contents', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('cart_id',255);

            $table->string('art',255);
            $table->string('brand',255);
            $table->string('name',255);

            $table->decimal('special_price', 10, 2)->unsigned();
            $table->decimal('price', 10, 2)->unsigned();            
            $table->decimal('qty', 10, 2)->unsigned();            

            $table->timestamps();

            $table->foreign('cart_id')
                  ->references('cart_id')->on('orders')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */


    public function down()
    {
        Schema::dropIfExists('order_contents');
    }


}
