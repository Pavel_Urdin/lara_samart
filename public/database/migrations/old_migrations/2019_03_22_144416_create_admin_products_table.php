<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('sku',255);
            $table->string('brand',255);
            $table->string('name',255);
            $table->decimal('price', 10, 2)->unsigned();            
            $table->decimal('special_price', 10, 2)->unsigned();
            $table->string('url_key')->nullable(false);
            $table->timestamps();
            
            //$table->primary('id');
            $table->unique('sku', 'brand'); 
        
            $table->engine = 'InnoDB';
            //$table->charset = 'utf8';

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
