
require('./Example/bootstrap');
require('./Example/bootstrap');

import Vue from "vue";
import App from "./App.vue";

import VueRouter from "vue-router";
import router from "./router.vue";

import Vuex from 'vuex';


Vue.config.productionTip = false;
Vue.use(VueRouter);
Vue.use(Vuex);

new Vue({
    render: h => h(App),
    router
}).$mount("#app");
