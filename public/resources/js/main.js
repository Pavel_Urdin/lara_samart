//require('./Example/bootstrap');

import Vue from "vue";
import App from "./App.vue";

//import VueRouter from "vue-router";

import CKEditor from '@ckeditor/ckeditor5-vue';

//import Vuex from 'vuex';
//Vue.use(Vuex);

import VueResource from "vue-resource";
Vue.config.productionTip = false;


//Vue.use(VueRouter);

Vue.use(VueResource);
Vue.use( CKEditor );

import router from "./router.vue";
import store from "./store/index.vue";


new Vue({

    render: h => h(App),
    router,
    store

}).$mount("#app");

