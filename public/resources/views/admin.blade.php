<!DOCTYPE HTML>
<html>
    <head>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
	<style>

	 body{
	     display:flex;
	     background-color:#f4f4f4;
	     height:100vh;
	     align-items: center;
	 }
	 .container{
	     width:90%;
	 }
	 .card{
	     display:flex;
	 }

	 .card.card-login .card-login-splash {
	     overflow: hidden;
	     position: relative;
	     z-index: 1;
	     display: flex;
	     align-items: center;
	     justify-content: center;
	     color: #fff;
	     flex: 1;
	 }
	 .card-content{
	     flex:1;
	 }

	</style>
    </head>

    <body>
	<div class="container">
	    <div class="row">
		<div class="col s8 offset-s2">
		    <div class="card card-login">

			<div class="card-login-splash">
			    <!--
			    <div class="wrapper">
				<h3>Account</h3>

				<a class="btn" href="#!">Sign In</a>
				<a class="btn" href="#!">Register</a>
			    </div>
			    -->
			    <img src="https://cdn.shopify.com/s/files/1/1775/8583/t/1/assets/geometric-cave.jpg?7999575290568706578" alt="">
			</div>

			<div class="card-content">
			    <span class="card-title">Log In</span>
			    <form>
				<div class="input-field">
				    <input id="username" type="text" class="validate">
				    <label for="username">Username</label>
				</div>
				<div class="input-field">
				    <input id="password" type="password" class="validate">
				    <label for="password">Password</label>
				</div>

				<a href="#!">Forgot Password?</a>

				<br><br>
				<div>
				    <input class="btn right" type="submit" value="Log In">
				    <a href="#!" class="btn-flat">Back</a>
				</div>

			    </form>
			</div>
		    </div>

		</div>
	    </div>
	</div>
    </body>


</html>
