<!DOCTYPE html>
<html>
<head>
	<title>Корзина</title>
	<meta charset="utf-8">
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/css/common.css" rel="stylesheet">
	<script src="/lib/jquery-2.2.3.min.js"></script>
</head>

<body>

	<style type="text/css">
		
		body{

			margin: auto;
			max-width: 1280px;
			/*background-color: white;
			-webkit-box-shadow: 0px 1px 4px 0px rgba(0,0,0,0.2);
    		box-shadow: 0px 1px 4px 0px rgba(0,0,0,0.2);*/
    		
    		
		}		


	</style>

	<?php 
		
		require_once("app/views/header.php");
		require_once("app/views/menu.php");
		require_once("app/views/cart.php");
		require_once("app/views/footer.php");

	?>
	
</body>