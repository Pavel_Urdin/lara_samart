
<!doctype html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
	<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css') }}">
	<script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>
	<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('css/header.css') }}">


       {{--           
	<link rel="stylesheet" href="{{ asset('css/product.css') }}">
       --}}

	<script>
	 $(document).ready(function(){
	     $('.tabs').tabs();
	 });
	</script>


    </head>

    <body>



	@include('header/header')

          
    <style type="text/css">


		.promos{

			width: 1200px;
			margin: auto;
			overflow: hidden;
			text-align: center;
			margin-bottom: 20px;


		}


		.promos > li {
			display: inline-block;
		    width: 32%;
		    margin-right: 1%;
		    list-style: none;
		    position: relative;
		    /*border: 1px solid #cccccc;*/
		}

		.promos > li:nth-child(3n) {
    		margin-right: 0;
		}

		.promos > li:hover{

			opacity: 0.8;

		}

		.promos img {
    		max-width: 100%;
    		width: 100%;
		}



		h2{

			margin: 0;
		    margin-bottom: 0.5em;
		    color: #636363;
		    font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
		    font-size: 24px;
		    font-weight: 400;
		    font-style: normal;
		    line-height: 1.2;
		    text-rendering: optimizeLegibility;
		    text-transform: uppercase;
		}


		h2.subtitle {
			margin: auto;
		    padding: 6px 0;
		    text-align: center;
		    color: #3399cc;
		    font-weight: 600;
		    border-bottom: 1px solid #cccccc;
		    border-top: 1px solid #cccccc;
		    cursor: pointer;
		    max-width:1200px; 
		}



		.products-grid{

			text-align: center;
			/*max-width: 1200px;*/
			margin: 20px 20px ;
		}


		.item{

			display: inline-block;
			width: 21.2%;
    			margin-right: 2.22222%;
    			border: 1px solid #ededed;
    			padding: 10px;
    			margin: 10px 10px;
    			border: 1px solid white;
			cursor: pointer;
		}

		.item:hover{
			border: 1px solid #3399cc;
			box-shadow: 0 1px 7px -1px rgba(0,0,0,.25);
			box-shadow: 0 1px 7px -1px rgba(51, 153, 204, 0.9);
			border-radius: 2px;
		    	/*text-decoration: none;*/
			/*border: 1px solid #3399cc;*/

			transform: scale(1.05);
			transition: all .15s linear;
			max-width: 100%;


		}

		.product-image{
			display: block;
			/*border: solid 1px #ededed;*/
			width: 100%;
			margin-bottom: 10px;


		}

		.product-image>img{

			max-width: 100%;

		}

		.product-image>img:hover{
			/*transform: scale(1.05);
			transition: all .15s linear;
			max-width: 100%;*/

		}

		.product-image:hover{

			/*border: 1px solid #3399cc;*/

		}

		.product-name{

			height: 36px;
			/*text-align: center;*/
			outline: hidden;
		}

		.product-name>a{

			text-transform: uppercase;
    		margin-bottom: 5px;
    		font-size: 14px;
    		font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
    		text-rendering: optimizeLegibility;
    		color: #636363;
    		font-weight: 500;

		}

		.product-name>a:hover{

			color:#3399cc;

		}

		.price{
			
		/*
			white-space: nowrap;
    		font-family: "Helvetica Neue", Verdana, Arial, sans-serif;
			color: #3399cc;
    		font-size: 16px;
		

		color: #e09d00;
    		font-size: 24px;
    		white-space: nowrap;
    		font-family: "Helvetica Neue", Verdana, Arial, sans-serif;
    		font-family: Georgia,"Times New Roman",Times,serif;
    		font-style: italic;
		*/


		}

		

		.regular-price{

    		color: #9e9e9e;
			font-family: Georgia,"Times New Roman",Times,serif;
    		font-style: italic;
    		text-decoration: line-through;
    		margin-right: 10px;
    		font-size: 18px;
		}

		.special-price{

    		white-space: nowrap;
    		font-family: "Helvetica Neue", Verdana, Arial, sans-serif;
			color: #3399cc;
    		
    		/*color: #3399cc;*/
			color: #e09d00;
    		font-size: 26px;
    		white-space: nowrap;
    		font-family: "Helvetica Neue", Verdana, Arial, sans-serif;
            font-family: Georgia,"Times New Roman",Times,serif;
    		font-style: italic;

		}


	</style>


	<ul class="products-grid">

            @isset($products)
            @foreach ($products as $prod)

	      <li class="item last">


	        <a href="/product/{{$prod->id}}" title="{{$prod->name}}" class="product-image">
	          <img src="/{{$prod->image}}" alt="{{$prod->name}}">
	        </a>


	        <h3 class="product-name">

		      <a href="/product/{{$prod->id}}">
                     {{$prod->name}}
		      </a>

            </h3>


	        <div class="price-box">

	   	       <span class="regular-price">
	              <span class="price">{{$prod->price}} грн</span>                                 
	           </span>

               @if ($prod->special_price)
	   	       <span class="special-price">
	             <span class="price">{{$prod->special_price}} грн</span>                                    
	           </span>
               @endif

	       </div>


          </li>

            @endforeach
            @endisset

        </ul>

	@include('footer/footer')

    </body>

</html>
