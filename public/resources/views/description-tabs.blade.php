
<div class="description-tabs">


    {{--
					     <input type="radio" name="tab" id="tab-1" checked>
					     <label class = "tabs tab1" for="tab-1">{{$description}}</label>

					     <input type="radio" name="tab" id="tab-2">
					     <label class = "tabs tab2" for="tab-2">Характеристики</label>

					     <input type="radio" name="tab" id="tab-3">
					     <label class = "tabs tab3" for="tab-3">Компоненты товара</label>

					     <input type="radio" name="tab" id="tab-4">
					     <label class = "tabs tab4" for="tab-4"><a href="/product#description">Отзывы</a></label>
					     --}}



    @isset($description)
     @foreach ($description as $data)
	<input type="radio" name="tab" id="tab-{{$loop->index}}" checked>
	<label class = "tabs tab1" for="tab-{{$loop->index}}"><a class="tab" href="{{$all->id}}#description{{$loop->index}}">{{$data->name}}</a></label>
     @endforeach
    @endisset
    

    <div class="content">

    @isset($description)
	@foreach ($description as $data)
	    <article class="tab-{{$loop->index}}">
		<h2 id="description{{$loop->index}}" class="descripti-header">{{$data->name}}</h2>
		{!!$data->data!!}
	    </article>
	@endforeach
    @endisset
    </div>



    <!--
	 <article class="tab-1">

	 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore numquam, itaque laboriosam debitis temporibus incidunt, dicta dignissimos sapiente voluptatibus iusto. Voluptate, laborum libero odio nostrum consequuntur aliquid molestiae soluta optio.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

	 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore numquam, itaque laboriosam debitis temporibus incidunt, dicta dignissimos sapiente voluptatibus iusto. Voluptate, laborum libero odio nostrum consequuntur aliquid molestiae soluta optio.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

	 </article>
	 <article class="tab-2">

	 <table class="data-table" id="product-attribute-specs-table">
	 <colgroup><col width="25%">
	 <col>
	 </colgroup>

	 <tbody>

	 <tr class="first odd">
	 <th class="label">Артикул</th>
	 <td class="data last">140F1235</td>
	 </tr>


	 <tr class="even">
	 <th class="label">Бренд</th>
	 <td class="data last">legrand</td>
	 </tr>

	 </tbody>
    	 </table>

	 </article>

	 <article class="tab-3">
	 <p style="text-align: justify;">Защита от снега и льда открытых площадок, предотвращение от промерзания металлических трубопроводов. «Тёплый пол»: применяется при бетонной основе пола для полногоили вспомогательного отопления. Поставляется с холодным соединительным проводом с герметичными переходной и концевой муфтами.</p>
	 <h2>Технические характеристики:</h2>
	 <table style="width: 588px;">
	 <tbody>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• тип кабеля</td>
	 <td style="font-weight: 400; width: 322px;">двухжильный экранированный</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• рабочее напряжение</td>
	 <td style="font-weight: 400; width: 322px;">- 220 ... 240 В</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• удельная мощность</td>
	 <td style="font-weight: 400; width: 322px;">18 Вт/м при 230 В-</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• диаметр внеш. оболочки</td>
	 <td style="font-weight: 400; width: 322px;">6,9 мм</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• мин. радиус изгиба</td>
	 <td style="font-weight: 400; width: 322px;">5 см</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• холодный проводник, питающий</td>
	 <td style="font-weight: 400; width: 322px;">2,3 м DTCL 3х1 ,5 мм2 (7 ... 90)м</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">нагревательный кабель</td>
	 <td style="font-weight: 400; width: 322px;">3х2,5 мм2 (105. ..1 70)м</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• цвет подсоединяемых</td>
	 <td style="font-weight: 400; width: 322px;">чёрный - фаза, голубой - нейтраль,</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">проводов</td>
	 <td style="font-weight: 400; width: 322px;">жёлто-зелёный - «земля»</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• экран</td>
	 <td style="font-weight: 400; width: 322px;">100%-ное покрытие; алюминиевая фольга с</td>
	 </tr>
	 <tr>
	 <td style="width: 250px;"> </td>
	 <td style="font-weight: 400; width: 322px;">дренажной лужёной медной жилой 0,5 мм2</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• внутренняя изоляция</td>
	 <td style="font-weight: 400; width: 322px;">сшитый полиэтилен РЕХ</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• наружная изоляция</td>
	 <td style="font-weight: 400; width: 322px;">поливинилхлорид 105°С PVC, огнеупорный, красный</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• максимальная температура</td>
	 <td style="font-weight: 400; width: 322px;">60°С/90°С во вкл./выкл. состоянии</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• допуски на сопротивление</td>
	 <td style="font-weight: 400; width: 322px;">-5% ... +10%</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• допуски на длину</td>
	 <td style="font-weight: 400; width: 322px;">-2% -10 см ... +2% +10 см</td>
	 </tr>
	 <tr>
	 <td style="font-weight: 400; width: 250px;">• сертифицирован</td>
	 <td style="font-weight: 400; width: 322px;">ТР те, IEC 800, DEMKO, SEMKO, СЕ, ЕАС</td>
	 </tr>
	 </tbody>
	 </table>
	 <p> </p>

	 </article>

	 <article id="description" class="tab-4">
	 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore numquam, itaque laboriosam debitis temporibus incidunt, dicta dignissimos sapiente voluptatibus iusto. Voluptate, laborum libero odio nostrum consequuntur aliquid molestiae soluta optio.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

	 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore numquam, itaque laboriosam debitis temporibus incidunt, dicta dignissimos sapiente voluptatibus iusto. Voluptate, laborum libero odio nostrum consequuntur aliquid molestiae soluta optio.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	 </article>
	 </div>
    -->

    <br class="clear:both" />

</div>

</div>
