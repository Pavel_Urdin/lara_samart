<div class="header-main">
      
    @include('header/logo-block')
    @include('header/menu-block')
    @include('header/search-block')
    @include('header/contact-block')
    @include('header/cart-block')
 
</div>
