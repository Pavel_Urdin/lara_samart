<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>

	<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css') }}">

	<script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>
	<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js') }}"></script>


	<!--
	     <script src="{{ asset('js/app.js') }}"></script>
	     <link rel="stylesheet" href="{{ asset('css/app.css') }}">
	-->

	<link rel="stylesheet" href="{{ asset('css/header.css') }}">


    </head>

    <body>

	@include('header/header')
	@include('main')
	@include('footer/footer')

	<script>
	 $(document).ready(function(){
	     $('.carousel').carousel();
	 });

	 $(document).ready(function(){

	     let carousel = $('.carousel').carousel({
		 numVisible:5,
		 dist:-350,
		 padding:0,
		 shift:0,
		 duration:150,
		 indicators:true,
	     });

	     function autoplay() {
		 setInterval(function(){
		     $('.carousel').carousel('next');
		 }, 6000);
	     }

	     autoplay()
	 });
	</script>

    </body>

</html>
