<section class="category-block">
  <h2 class = "category-head">Выбор по категориям</h2> 

  <ul class="category-list">
    <li class = "category-tab">
      <a class="category-link">
	<img class ="category-img" src="images/cat_1.webp">
	<p class ="category-name">
	  Системы домашнего комфорта
	</p>
      </a>
    </li>
    <li class = "category-tab">
      <a class="category-link">
	<img class ="category-img" src="images/cat_2.webp">
	<p class ="category-name">
	  Установочное электрооборудование
	</p>
      </a>
    </li>
    <li class = "category-tab">
      <a class="category-link">
	<img class ="category-img" src="images/cat_3.webp">
	<p class ="category-name">
	  Защитно-коммутационное оборудование
	</p>
      </a>
    </li>
    <li class = "category-tab">
      <a class="category-link">
	<img class ="category-img" src="images/cat_1.webp">
	<p class ="category-name">
	  Источники бесперебойного питания
	</p>
      </a>
    </li>
  </ul>


  <ul class="category-list">
    <li class = "category-tab">
      <a class="category-link">
	<img class ="category-img" src="images/cat_5.webp">
	<p class ="category-name">
	  Системы домашнего комфорта
	</p>
      </a>
    </li>
    <li class = "category-tab">
      <a class="category-link">
	<img class ="category-img" src="images/cat_6.webp">
	<p class ="category-name">
	  Установочное электрооборудование
	</p>
      </a>
    </li>
    <li class = "category-tab">
      <a class="category-link">
	<img class ="category-img" src="images/cat_7.webp">
	<p class ="category-name">
	  Защитно-коммутационное оборудование
	</p>
      </a>
    </li>
    <li class = "category-tab">
      <a class="category-link">
	<img class ="category-img" src="images/cat_8.webp">
	<p class ="category-name">
	  Источники бесперебойного питания
	</p>
      </a>
    </li>
  </ul>
</section>
