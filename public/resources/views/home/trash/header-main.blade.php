<div class="header-main">
    @include('logo-block')
    @include('menu-block')
    @include('search-block')
    @include('contact-block')
    @include('cart-block')
</div>
