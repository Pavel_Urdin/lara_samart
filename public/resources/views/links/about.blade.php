<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>

	<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css') }}">

	<script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>
	<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js') }}"></script>


	<!--
	     <script src="{{ asset('js/app.js') }}"></script>
	     <link rel="stylesheet" href="{{ asset('css/app.css') }}">
	-->

	<link rel="stylesheet" href="{{ asset('css/header.css') }}">


    </head>

    <body>




	@include('header/header')


    <style>  

    h1{


        font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
        margin: 20px 0px;
        line-height: 24px;
        color: #636363;
        font-size: 24px;
        font-weight: 600;
    }

    h2{

        font-size: 20px;
        font-weight: 600;
        margin-bottom: 20px;
    }


    h3{

        font-weight: 600;
        margin:10px 0px;

    }

    .cpt_maincontent li{

        margin: 15px 0px;
        font-weight: 600;
        font-size: 20px;

    }

    .cpt_maincontent {

        font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
        margin-top: 20px;
        color: #636363;
        line-height: 24px;
        margin-bottom: 20px;
    }

    </style>
      
<body>


<div class="cpt_maincontent">

    <h1><img
                src="http://smartelectric.com.ua/published/publicdata/STORE/attachments/SC/images/1_logo.jpg"
                width="225" height="95" style="margin: 15px 50px; float: left;">
    </h1>


    <p><h2>Интернет-магазин Smart-Electric&nbsp;является интернет-проектом&nbsp;ООО "Спец-Электро-Сервис".</h2>
    </p>

    <p>ООО "Спец-Электро-Сервис"&nbsp;является специализированной компанией по продаже продукции производства компании Legrand (Франция) и ее авторизированным дистрибьютором с 2006г.
    </p>

    <p>Все предлагаемые изделия оригинальные, и поставляются со склада официального представительства&nbsp;Legrand&nbsp;в Украине, компании&nbsp;Легранд-Украина.
    </p>

    <p>Вся продукция соответствует всем принятым нормам и стандартам, имеет гарантированный срок эксплуатации и сертификаты.
    </p>

    <p>Ознакомится с продукцией&nbsp;Legrand можно посетив наш выставочный зал, расположенный по адресу: г.Днепропетровск, проспект Пушкина, 73</span>
    </p>



    <table border="0" align="center">
        <tbody>
        <tr>
            <td>
                <img src="http://smartelectric.com.ua/published/publicdata/STORE/attachments/SC/images/sertificate-legrand.jpg"
                     width="279" height="400" style="display: block; margin: 10px 20px; vertical-align: text-top;"
                     alt="Сертификат официального дистрибьютора Legrand"
                     title="Сертификат официального дистрибьютора Legrand"></td>
            <td><img src="http://smartelectric.com.ua/published/publicdata/STORE/attachments/SC/images/sert%20Hager.jpg"
                     width="287" height="400" alt="Сертификат официального дистрибьютора концерна Hager"
                     title="Сертификат официального дистрибьютора концерна Hager"
                     style="margin: 10px 20px; vertical-align: text-top;"></td>
        </tr>
        </tbody>
    </table>
    <div>
        <h2 class="sub_header">Интернет-магазин Smart Electric — официальный дистрибьютор компании Legrand в Украине.</h2>
        <p> Гарантия качества — это официальный сертификат дистрибьютора компании Legrand!</p>
        <p>Только сертификат&nbsp;дистрибьютора &nbsp;с которым вы можете ознакомиться, гарантирует качество товара. Отсутствие сертификата у продавца, свидетельствует о том, что он не может гарантировать продажу официальных товаров.
        </p>

    </div>

</div>

</body>



@include('footer/footer')











