<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>

	<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css') }}">

	<script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>
	<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js') }}"></script>


	<!--
	     <script src="{{ asset('js/app.js') }}"></script>
	     <link rel="stylesheet" href="{{ asset('css/app.css') }}">
	-->


	<link rel="stylesheet" href="{{ asset('css/header.css') }}">
    <link rel="stylesheet" href="{{ asset('css/cart.css') }}">


    </head>



    <style>

        h1{

            font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
            color: #636363;
            font-weight: 400;
            font-size: 25px;
            text-transform: uppercase;
            padding-left: 15px;
            padding-right: 15px;
        }


        .category-title{

            margin-bottom: 20px;
            margin-top: 20px;

        }


        .contact-map-wrap{

            box-sizing: border-box;
            position: relative;
            float:left;
            width: 50%;
            padding-left: 15px;
            padding-right: 15px;


        }


        #map {
            /*padding: 5px;*/
            height: 350px;
            border: 6px solid #ececec;
            margin-bottom: 20px;
        }

        .map{
            width:100%;
            height:350px;
            margin:0px;
            bottom: 0px;
        }



        .contact-form{
            box-sizing: border-box;
            float:left;
            position:relative;
            width: 50%;
            font-family: "Helvetica Neue", Verdana, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.5;
            /*text-align: center;*/


        }

        .form-wrapper{

            margin-bottom: 20px;

        }

        .form-wrapper .input-box .input-text {
            box-sizing: border-box;
            width: 100%;
            color: #636363;
        }


        .field{
            margin-bottom: 15px;
            max-width: 400px;
        }




        textarea{
            padding: 5px 10px;
            border: 1px solid silver;
            background: #FFFFFF;
            min-height: 150px;
        }

        .buttons-set>button{

            display: inline-block;
            background: #3399cc;
            padding: 7px 15px;
            border: 0;
            color: #FFFFFF;
            font-size: 13px;
            font-weight: normal;
            font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
            line-height: 19px;
            text-align: center;
            text-transform: uppercase;
            vertical-align: middle;
            white-space: nowrap;
        }

        .buttons-set>button:hover {
            background: #2e8ab8;
            cursor: pointer;
            border: 0;
            border-style: none;
        }


        .contacts{

            position: relative;
            font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
            line-height: 24px;
            margin-bottom: 20px;
            color: #636363;

        }

        .contacts strong{

            font-weight: 600;

        }


        .address{
            /*float:left;*/
            display: inline-block;
            margin-right: 30px;

        }
        
        .telefon {
            /*line-height:30px;*/
            display: inline-block;
            /*float: left;*/
            max-width: 180px;

        }

        .telefon div{
            display: inline-block;
            vertical-align: middle;
            height: 31px;
            margin-bottom: 8px;
            line-height: 31px;
        }

        .telefon>p{

            /*display: table-row;*/
            text-align: left;
            valign: middle;
            margin-bottom: 11px;

        }

        .telefon>p>img{

            /*display: table-cell;*/
            width: 31px;
            height: 31px;

        }

        .telefon>p>div{

            margin-bottom: 5px;

        }


        .telefon>p>span{
            /*display: table-cell;*/

            /*line-height: 25px;*/
            padding: 50px 0px;
            margin-bottom: 20px;
        }





    </style>


@include('header/header')


<div id="map" style="position: relative; overflow: hidden;">

                   <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d786.4803710787647!2d35.014616080057166!3d48.468153865755674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40dbe2ffb8582deb%3A0xf2251a88f4627a30!2z0L_RgNC-0YHQv9C10LrRgiDQn9GD0YjQutGW0L3QsCwgNzMsINCU0L3RltC_0YDQvsyBLCDQlNC90ZbQv9GA0L7Qv9C10YLRgNC-0LLRgdGM0LrQsCDQvtCx0LvQsNGB0YLRjA!5e0!3m2!1sru!2sua!4v1502864147505"  frameborder="0" style="border:0" allowfullscreen>                      </iframe>

</div>

    
<div class="address">
<p><strong>Адрес:</strong>
<br> г. Днепропетровск, пр. Пушкина 73</p>
<p><strong>Время работы:</strong><br>
Пн-Пт — с 9:00 до 18:00<br> Сб — с 10:00 до 15:00<br> Вс — выходной</p>
</div>





<div class="telefon">
<p><strong>Контактные телефоны:</strong></p>
<div>
<div><img src="http://smartelectric.com.ua/published/publicdata/STORE/attachments/SC/images/1mts.jpg" width="31" height="31" alt="Для абонентов МТС " title="МТС"></div>
<div>(050) 716-51-15</div>
</div>
                    <div>
                        <div><img src="http://smartelectric.com.ua/published/publicdata/STORE/attachments/SC/images/1k_s.jpg" width="31" height="31" alt="Для абонентов Киевстар" title="Киевстар"></div>
                        <div>(050) 716-51-15</div>
                    </div>
                    <div>
                        <div><img src="http://smartelectric.com.ua/published/publicdata/STORE/attachments/SC/images/1life_logo_200.jpg" width="29" height="29" alt="Для абонентов Лайф" title="Лайф" style="border: 1px solid pink;"></div>
                        <div>(050) 716-51-15</div>
                    </div>
                </div>






                <!--
                <div class="telefon">
                    <p><strong>Контактные телефоны:</strong></p>
                    <p><img src="http://smartelectric.com.ua/published/publicdata/STORE/attachments/SC/images/1mts.jpg" width="31" height="31" alt="Для абонентов МТС " title="МТС">&nbsp;&nbsp;<span>(050) 716-51-15</span></p>
                    <p><span><img src="http://smartelectric.com.ua/published/publicdata/STORE/attachments/SC/images/1k_s.jpg" width="31" height="31" alt="Для абонентов Киевстар" title="Киевстар">&nbsp;&nbsp;(097) 716-51-15</span></p>
                    <p><span><span><img src="http://smartelectric.com.ua/published/publicdata/STORE/attachments/SC/images/1life_logo_200.jpg" width="29" height="29" alt="Для абонентов Лайф" title="Лайф" style="border: 1px solid pink;">&nbsp;&nbsp;(063) 716-51-15</span></span></p>
                </div>
                -->




            </div>
                    
@include('footer/footer')
