<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>

	<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css') }}">

	<script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>
	<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js') }}"></script>


	<!--
	     <script src="{{ asset('js/app.js') }}"></script>
	     <link rel="stylesheet" href="{{ asset('css/app.css') }}">
	-->

	<link rel="stylesheet" href="{{ asset('css/header.css') }}">


    </head>

    <body>

	@include('header/header')


    <style>  

    h1{


        font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
        margin: 20px 0px;
        line-height: 24px;
        color: #636363;
        font-size: 24px;
        font-weight: 600;
    }

    h2{

        font-size: 20px;
        font-weight: 600;
        margin-bottom: 20px;
    }


    h3{

        font-weight: 600;
        margin:10px 0px;

    }

    .cpt_maincontent li{

        margin: 15px 0px;
        font-weight: 600;
        font-size: 20px;

    }

    .cpt_maincontent {

        font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
        margin-top: 20px;
        color: #636363;
        line-height: 24px;
        margin-bottom: 20px;
    }

    </style>
      
    <div class="cpt_maincontent">

    <h1>Информация об оплате</h1>
    <p>К оплате принимаются наличные и безналичные денежные средства.&nbsp;<span
                style="font-size: medium;">Оплата производится в национальной валюте Украины - гривне.</p>
    <p></p>
    <h1>Возможны два ваианта:</h1>
    <p></p>
    <ul>
        <li>Оплата наличными</li>
    </ul>
    <div>Уважаемые клиенты, оплатить заказ наличными деньгами Вы можете&nbsp;<strong>только</strong>&nbsp;в нашем офисе по адресу: г.Днепропетровск, пр. Пушкина 73
    </div>
    <ul>
        <li>Оплата по безналичному расчету</li>
    </ul>
    <div>
        <h3>Безналичный расчёт для юридических лиц</h3>
        <p>При оформлении заказа клиентами-юридическими лицами формируется счёт на оплату, который передаётся клиенту средствами связи. После оплаты счёта производится доставка товара и пакета всех необходимых для бухгалтерии документов.
        </p>
        <p></p>
        <h3>Безналичный расчёт для физических лиц</h3>
        <p>Заказ сформированный на физическое лицо можно оплатить безналичным банковским переводом через любой банк, действующий на территории Украины. При этом банком будет удержана комиссия в соответствии со своими внутренними тарифами, обычно это около 1% от суммы перевода.
        </p>

        <h1>Срок действия счёта-фактуры составляет 1 банковский день от даты выписки.</h1>
        <p></p>
        <h1>В случае оплаты просроченного счёта необходимо связаться с нами для уточнения цены и наличия товара на складе.
        </h1>

    </div>
</div>

@include('footer/footer')
