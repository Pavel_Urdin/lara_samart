<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>

	<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css') }}">

	<script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>
	<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js') }}"></script>


	<!--
	     <script src="{{ asset('js/app.js') }}"></script>
	     <link rel="stylesheet" href="{{ asset('css/app.css') }}">
	-->


	<link rel="stylesheet" href="{{ asset('css/header.css') }}">
    <link rel="stylesheet" href="{{ asset('css/cart.css') }}">


    </head>





   <style>
        
    .regheader{

        font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
        margin: 20px 0px;
        line-height: 24px;
        color: #636363;

    }

    .regheader h1{

        font-size: 24px;
        font-weight: 600;
        margin-bottom: 10px;

    }

    </style>

@include('header/header')

<div class="regheader">
    <p></p>
    <h1>Информация о доставке по Украине</h1>
    <p></p>
    <p><span style="font-size: medium;">Для получения продукции с доставкой по Украине нужно:</span></p>
    <p><span style="font-size: medium;">• получить от нас счёт-фактуру на выбранный Вами товар</span></p>
    <p><span style="font-size: medium;">• оплатить счёт-фактуру в любом банке</span></p>
    <p><span style="font-size: medium;">• после поступления средств на расчётный счет предприятия, мы высылаем товар в Ваш город через компанию перевозчика которого Вы укажите,&nbsp;сообщаем Вам номер транспортной накладной и ориентировочную дату прихода товара в Ваш город</span></p>
    <p><span style="font-size: medium;">• Вы получаете груз с документами, удостоверяющими Вашу личность, и оплачиваете доставку посылки согласно тарифам перевозчика.</span></p>
    <p><span style="font-size: medium;"><br></span></p>
    <p></p>
    <h1>Информация о доставке по Днепропетровску</h1>
    <p></p>
    <p><span style="font-size: medium;">Адресная доставка товара производится при заказе партии продукции на суму не менее 500 грн., при этом минимальная стоимость доставки составит 50 грн.</span></p>
    <p></p>
    <p ><span style="font-size: medium;">Стоимость услуги может варьироваться в зависимости от объема партии товара и удалённости места доставки.</span></p>
    <p></p>
    <p ><span style="font-size: medium;">Точную стоимость доставки и сроки можно будет уточнить при заказе.</span></p>
    <p></p>
    <p ><span style="font-size: medium;">Для получения продукции с доставкой по Днепропетровску нужно:</span></p>
    <p></p>
    <p><span style="font-size: medium;">• получить от нас счёт-фактуру на выбранный Вами товар</span></p>
    <p></p>
    <p><span style="font-size: medium;">• оплатить счёт-фактуру в любом банке</span></p>
    <p></p>
    <p><span style="font-size: medium;">• после поступления средств на расчётный счет предприятия, мы свяжемся с Вами и согласуем время доставки.</span></p>
    <p></p>
    <div ></div>
    &nbsp;
    <p><span style="font-size: x-large; ">Срок действия счёта-фактуры составляет 1 банковский день от даты выписки.</span></p>
    <p><span style="font-size: x-large; ">В случае оплаты просроченного счёта необходимо связаться с нами для уточнения цены и наличия товара на складе.</span></p>
</div>






  
@include('footer/footer')
