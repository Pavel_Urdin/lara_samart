<!doctype html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>



	<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css') }}">
	<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js') }}"></script>
	<script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>

	<!-- Плагин для PopUp -->
	<script src="{{ asset('js/jquery.bpopup.min.js') }}"></script>

	<link rel="stylesheet" href="{{ asset('css/header.css') }}">
	<link rel="stylesheet" href="{{ asset('css/product.css') }}">


    </head>

    <body>


	{{--  {{$all}}    --}}

	@include('header/header')

	<div class="col-main">
	    <div class="product-wrapper">

		<div class="product-image-box">

		    <div class="image-grid">
			<ul class="slides-grid">
			    @isset($images)
			    @foreach ($images as $img)
				@if ($loop->index == 0)
				    <li class="slick-slide active">
					<img width="50" height="50" src={{"/".$img}}>
				    </li>
				    @continue
				@endif
				<li class="slick-slide">
				    <img width="50" height="50" src={{"/".$img}}>
				</li>
			    @endforeach
                            @endisset
			</ul>
		    </div>

		    <div class = "product-image">
			<img id="image-main" class="gallery-image visible" src={{"/".$imgDefalt}} alt="Фотография готовится">
		    </div>

		</div>





            <div class = "product-shop">
		    <div class="product-name">
			<h1>{{$all['name']}}</h1>
		    </div>


            @if ($all['special_price']>0)

		      <div class="product-special-price-container">
                      <span class="product-price">{{$all['price']}} грн.</span>
		      </div>

              <div class="product-price-container">
                      <span class="product-price">{{$all['special_price']}} грн.</span>
              </div>

            @else

		    <div class="product-price-container">
                      <span class="product-price">{{$all['price']}} грн.</span>
		    </div>

            @endif



		    <div class="service-information">

            {{--

			<ul class="information-block">

			    <li class="delivery-inf">Условия оплаты и доставки</li>
			    <li class="timetable-inf">График работы</li>
			    <li class="contact-inf">Адрес и контакты</li>

			</ul>

              --}}


		    </div>




		    {{--
		    <form id="frm" name="prod">
			--}}



			<form id="frm" name="prod" enctype="multipart/form-data" action="/checkout/cart" method="POST">


                    {{--           

		            <input type=hidden name=art value={{$all['art']}}>
		            <input type="text" name="qty" id="qty" maxlength="12" value="1" title="Qty" class="input-text qty">

                     --}}           

			        <div class="add-to-cart">
			          <div class="qty-wrapper">
				    <label for="qty">Кол-во:</label>


				  <input type=hidden name=art value={{$all['art']}}>
				  <input type="text" name="qty" id="qty" maxlength="12" value="1" title="Qty" class="input-text qty">

                                            
			        </div>
			    </div>
			</form>


			<button class="button btn-cart">
			    <span>
				<span>Добавить в корзину</span>
			    </span>
			</button>


			<div class="subproducts">
			    <ul class="subproducts-grid">
				@isset($components)
				@foreach ($components as $cmp)
				    @if ($loop->index == 0)
					<li class="subproduct">
					    <img width="100" height="100" src={{"/".$cmp->image}}>
					    <div>{{$cmp->name}}</div>
					</li>
					@continue
				    @endif
				    <div class="plus">+</div>
				    <li class="subproduct">
					<img width="100" height="100" src={{"/".$cmp->image}}>
					<div>{{$cmp->name}}</div>
				    </li>
				@endforeach
				@endisset
			    </ul>
			</div>


		</div>
	    </div>






         @include('description-tabs')
         {{--   
	    <h2 class="subtitle">Вам также понравится</h2>
           --}}  
	</div>
	</div>


	@include('footer/footer')




     <!--  
      <script src="/../../../lib/jquery.bpopup.min.js"></script>
     -->
               

	<script type="text/javascript">

	 var $ = jQuery.noConflict();


     //Изменение картинки 

	 $(document).ready(function() {

	     $( ".slick-slide" ).on( "mouseover", function() {

 		 var el = $(this);
 		 var img = $(el).children().attr('src');
 		 var screen = $('.gallery-image').attr('src', img);

 		 var cls =  $($(this)[0]).attr('class');

 		 if (cls.indexOf('active') === -1){

 		     $('.active').attr('class','slick-slide');
 		     $(this).attr('class','slick-slide active');

 		 }
	     });

	 });
     
	</script>




	<script type="text/javascript">


	 $(document).ready(function(){




	     //Три кнопки
	     /*

	     popup = function(param){

		   $('.'+param+'-inf').on('click',function(){

		     $('.popup-'+param).bPopup({
			     modalClose: true,
		             opacity: 0.82,
		             speed: 650,
	                     transition: 'slideIn',
		    	     transitionClose: 'slideBack'
	             });

		   });
             };


	     popup('delivery');
	     popup('timetable');
	     popup('contact');

         */

	     
	     $('.btn-cart').on('click',function(){

           frm = $('#frm').serialize();


           //console.log(frm); 

          /*
		  $.ajax({
		     type: "POST",
             //url: "/checkout/cart",
		     url: "/checkout/addToCart",
             data: {
               cart:frm,
               crt:getCart(),
             },
		     success: function(msg){
                 console.log(msg); 
                 //console.log(getCart());
                 //document.location = '/checkout/cart';
		     }
		  });
          */




		 function getSku(){
		     var frm = $('#frm').serialize();
		     return (frm.substring(frm.indexOf('=')+1,frm.indexOf('&')));
		 }


		 function getQty(){
		     var frm = $('#frm').serialize();
		     return (frm.substring(frm.indexOf('&')+5));
		 }


		 function addToCart(){

		     var cart = getCart();

		     if(cart){

			   var sku = getSku();
			   var qty = getQty();

			   cart[sku] = qty;
			   setCart(cart);


		     }else{

			   cartInit();
			   addToCart();

		     }
		 }


		 function getCart(){

		     var cart;
		     cart = getCookie('cart');
		     if(cart){

			 cart = decodeCart(cart);
			 cart = unserialize(cart);

			 return cart;

		     }else{

			   return false;

		     }
		 }


		 function setCart(cart){

		     options = {expires:3600*24*7,path:"/"}

             cart = serialize(cart);
             cart = encodeCart(cart);

		     setCookie('cart', cart, options);

		 }


		 function cartInit(){

		     var cart=[];

		     var rand = Math.floor(Math.random()* 10000000);
		     cart['cartid'] = rand;
		     setCart(cart);
		     return cart;

		 }


		 function getCookie(name) {

		     var matches = document.cookie.match(new RegExp(
			 "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
		     ));

		     return matches ? decodeURIComponent(matches[1]) : undefined;
		 }



		 function setCookie(name, value, options) {

		     options = options || {};

		     var expires = options.expires;

		     if (typeof expires == "number" && expires) {

			   var d = new Date();
			   d.setTime(d.getTime() + expires * 1000);
			   expires = options.expires = d;

		     }

		     if (expires && expires.toUTCString) {
			   options.expires = expires.toUTCString();
		     }

		     value = encodeURIComponent(value);

		     var updatedCookie = name + "=" + value;

		     for (var propName in options) {

			   updatedCookie += "; " + propName;
			   var propValue = options[propName];
			   if (propValue !== true) {
			     updatedCookie += "=" + propValue;
			   }
		     }

		     document.cookie = updatedCookie;
		 }


                 //base64  
		 function decodeCart(data){
		     return window.atob(data); 
		 }

                 //base64  
		 function encodeCart(data){
		     return window.btoa(data);
		 }



		 function serialize (mixedValue) {
		     //  discuss at: http://locutus.io/php/serialize/
		     // original by: Arpad Ray (mailto:arpad@php.net)
		     // improved by: Dino
		     // improved by: Le Torbi (http://www.letorbi.de/)
		     // improved by: Kevin van Zonneveld (http://kvz.io/)
		     // bugfixed by: Andrej Pavlovic
		     // bugfixed by: Garagoth
		     // bugfixed by: Russell Walker (http://www.nbill.co.uk/)
		     // bugfixed by: Jamie Beck (http://www.terabit.ca/)
		     // bugfixed by: Kevin van Zonneveld (http://kvz.io/)
		     // bugfixed by: Ben (http://benblume.co.uk/)
		     // bugfixed by: Codestar (http://codestarlive.com/)
		     //    input by: DtTvB (http://dt.in.th/2008-09-16.string-length-in-bytes.html)
		     //    input by: Martin (http://www.erlenwiese.de/)
		     //      note 1: We feel the main purpose of this function should be to ease
		     //      note 1: the transport of data between php & js
		     //      note 1: Aiming for PHP-compatibility, we have to translate objects to arrays
		     //   example 1: serialize(['Kevin', 'van', 'Zonneveld'])
		     //   returns 1: 'a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}'
		     //   example 2: serialize({firstName: 'Kevin', midName: 'van'})
		     //   returns 2: 'a:2:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";}'

		     var val, key, okey
		     var ktype = ''
		     var vals = ''
		     var count = 0

		     var _utf8Size = function (str) {
			 var size = 0
			 var i = 0
			 var l = str.length
			 var code = ''
			 for (i = 0; i < l; i++) {
			     code = str.charCodeAt(i)
			     if (code < 0x0080) {
			         size += 1
			     } else if (code < 0x0800) {
			         size += 2
			     } else {
			         size += 3
			     }
			 }
			 return size
		     }

		     var _getType = function (inp) {
			 var match
			 var key
			 var cons
			 var types
			 var type = typeof inp

			 if (type === 'object' && !inp) {
			     return 'null'
			 }

			 if (type === 'object') {
			     if (!inp.constructor) {
			         return 'object'
			     }
			     cons = inp.constructor.toString()
			     match = cons.match(/(\w+)\(/)
			     if (match) {
			         cons = match[1].toLowerCase()
			     }
			     types = ['boolean', 'number', 'string', 'array']
			     for (key in types) {
			         if (cons === types[key]) {
			             type = types[key]
			             break
			         }
			     }
			 }
			 return type
		     }

		     var type = _getType(mixedValue)

		     switch (type) {
			 case 'function':
			     val = ''
			     break
			 case 'boolean':
			     val = 'b:' + (mixedValue ? '1' : '0')
			     break
			 case 'number':
			     val = (Math.round(mixedValue) === mixedValue ? 'i' : 'd') + ':' + mixedValue
			     break
			 case 'string':
			     val = 's:' + _utf8Size(mixedValue) + ':"' + mixedValue + '"'
			     break
			 case 'array':
			 case 'object':
			     val = 'a'
			     /*
				if (type === 'object') {
			        var objname = mixedValue.constructor.toString().match(/(\w+)\(\)/);
			        if (objname === undefined) {
			        return;
			        }
			        objname[1] = serialize(objname[1]);
			        val = 'O' + objname[1].substring(1, objname[1].length - 1);
				}
			      */

			     for (key in mixedValue) {
			         if (mixedValue.hasOwnProperty(key)) {
			             ktype = _getType(mixedValue[key])
			             if (ktype === 'function') {
					 continue
			             }

			             okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key)
			             vals += serialize(okey) + serialize(mixedValue[key])
			             count++
			         }
			     }
			     val += ':' + count + ':{' + vals + '}'
			     break
			 case 'undefined':
			 default:
			     // Fall-through
			     // if the JS object has a property which contains a null value,
			 // the string cannot be unserialized by PHP
			     val = 'N'
			     break
		     }
		     if (type !== 'object' && type !== 'array') {
			 val += ';'
		     }

		     return val
		 }


		 function unserialize (data) {
		     //  discuss at: http://locutus.io/php/unserialize/
		     // original by: Arpad Ray (mailto:arpad@php.net)
		     // improved by: Pedro Tainha (http://www.pedrotainha.com)
		     // improved by: Kevin van Zonneveld (http://kvz.io)
		     // improved by: Kevin van Zonneveld (http://kvz.io)
		     // improved by: Chris
		     // improved by: James
		     // improved by: Le Torbi
		     // improved by: Eli Skeggs
		     // bugfixed by: dptr1988
		     // bugfixed by: Kevin van Zonneveld (http://kvz.io)
		     // bugfixed by: Brett Zamir (http://brett-zamir.me)
		     //  revised by: d3x
		     //    input by: Brett Zamir (http://brett-zamir.me)
		     //    input by: Martin (http://www.erlenwiese.de/)
		     //    input by: kilops
		     //    input by: Jaroslaw Czarniak
		     //      note 1: We feel the main purpose of this function should be
		     //      note 1: to ease the transport of data between php & js
		     //      note 1: Aiming for PHP-compatibility, we have to translate objects to arrays
		     //   example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}')
		     //   returns 1: ['Kevin', 'van', 'Zonneveld']
		     //   example 2: unserialize('a:2:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";}')
		     //   returns 2: {firstName: 'Kevin', midName: 'van'}

		     var $global = (typeof window !== 'undefined' ? window : GLOBAL)

		     var utf8Overhead = function (chr) {
			 // http://locutus.io/php/unserialize:571#comment_95906
			 var code = chr.charCodeAt(0)
			 var zeroCodes = [
			     338,
			     339,
			     352,
			     353,
			     376,
			     402,
			     8211,
			     8212,
			     8216,
			     8217,
			     8218,
			     8220,
			     8221,
			     8222,
			     8224,
			     8225,
			     8226,
			     8230,
			     8240,
			     8364,
			     8482
			 ]
			 if (code < 0x0080 || code >= 0x00A0 && code <= 0x00FF || zeroCodes.indexOf(code) !== -1) {
			     return 0
			 }
			 if (code < 0x0800) {
			     return 1
			 }
			 return 2
		     }
		     var error = function (type,
					   msg, filename, line) {
			 throw new $global[type](msg, filename, line)
		     }
		     var readUntil = function (data, offset, stopchr) {
			 var i = 2
			 var buf = []
			 var chr = data.slice(offset, offset + 1)

			 while (chr !== stopchr) {
			     if ((i + offset) > data.length) {
			         error('Error', 'Invalid')
			     }
			     buf.push(chr)
			     chr = data.slice(offset + (i - 1), offset + i)
			     i += 1
			 }
			 return [buf.length, buf.join('')]
		     }
		     var readChrs = function (data, offset, length) {
			 var i, chr, buf

			 buf = []
			 for (i = 0; i < length; i++) {
			     chr = data.slice(offset + (i - 1), offset + i)
			     buf.push(chr)
			     length -= utf8Overhead(chr)
			 }
			 return [buf.length, buf.join('')]
		     }
		     var _unserialize = function (data, offset) {
			 var dtype
			 var dataoffset
			 var keyandchrs
			 var keys
			 var contig
			 var length
			 var array
			 var readdata
			 var readData
			 var ccount
			 var stringlength
			 var i
			 var key
			 var kprops
			 var kchrs
			 var vprops
			 var vchrs
			 var value
			 var chrs = 0
			 var typeconvert = function (x) {
			     return x
			 }

			 if (!offset) {
			     offset = 0
			 }
			 dtype = (data.slice(offset, offset + 1)).toLowerCase()

			 dataoffset = offset + 2

			 switch (dtype) {
			     case 'i':
			         typeconvert = function (x) {
			             return parseInt(x, 10)
			         }
			         readData = readUntil(data, dataoffset, ';')
			         chrs = readData[0]
			         readdata = readData[1]
			         dataoffset += chrs + 1
			         break
			     case 'b':
			         typeconvert = function (x) {
			             return parseInt(x, 10) !== 0
			         }
			         readData = readUntil(data, dataoffset, ';')
			         chrs = readData[0]
			         readdata = readData[1]
			         dataoffset += chrs + 1
			         break
			     case 'd':
			         typeconvert = function (x) {
			             return parseFloat(x)
			         }
			         readData = readUntil(data, dataoffset, ';')
			         chrs = readData[0]
			         readdata = readData[1]
			         dataoffset += chrs + 1
			         break
			     case 'n':
			         readdata = null
			         break
			     case 's':
			         ccount = readUntil(data, dataoffset, ':')
			         chrs = ccount[0]
			         stringlength = ccount[1]
			         dataoffset += chrs + 2

			         readData = readChrs(data, dataoffset + 1, parseInt(stringlength, 10))
			         chrs = readData[0]
			         readdata = readData[1]
			         dataoffset += chrs + 2
			         if (chrs !== parseInt(stringlength, 10) && chrs !== readdata.length) {
			             error('SyntaxError', 'String length mismatch')
			         }
			         break
			     case 'a':
			         readdata = {}

			         keyandchrs = readUntil(data, dataoffset, ':')
			         chrs = keyandchrs[0]
			         keys = keyandchrs[1]
			         dataoffset += chrs + 2

			         length = parseInt(keys, 10)
			         contig = true

			         for (i = 0; i < length; i++) {
			             kprops = _unserialize(data, dataoffset)
			             kchrs = kprops[1]
			             key = kprops[2]
			             dataoffset += kchrs

			             vprops = _unserialize(data, dataoffset)
			             vchrs = vprops[1]
			             value = vprops[2]
			             dataoffset += vchrs

			             if (key !== i) {
					 contig = false
			             }

			             readdata[key] = value
			         }

			         if (contig) {
			             array = new Array(length)
			             for (i = 0; i < length; i++) {
					 array[i] = readdata[i]
			             }
			             readdata = array
			         }

			         dataoffset += 1
			         break
			     default:
			         error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype)
			         break
			 }
			 return [dtype, dataoffset - offset, typeconvert(readdata)]
		     }

		     return _unserialize((data + ''), 0)[2]
		 }


		 addToCart();


		 $.ajax({

		     type: "POST",
		     url: "/checkout/addToCart",
             data: {
               cart:getCart(),
             },

		     success: function(msg){
                 console.log(msg); 
                 document.location = '/checkout/cart';
		     }
		  });


	     })

	 })

	</script>


    </body>

</html>
