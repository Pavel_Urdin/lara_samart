<style type="text/css">

 .main{
     margin-bottom: 50px;
 }


 .breadcrumbs{
     position: relative;
     width: 1200px;
     margin: 10px auto;
     font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
     text-transform: uppercase;
 }


 .breadcrumbs>ul>li{
     font-size: 12px;
     color: #636363;
     display: inline;
 }

 .breadcrumbs>ul>li>a:visited{

     color: #636363;

 }

 .breadcrumbs>ul>li>a:hover{

     text-decoration: underline;
     color: #3399cc;

 }

 .breadcrumbs>ul>li>span {

     padding: 10px;

 }

 .col-main {

     max-width: 1200px;
     margin: 10px auto;
     outline: hidden;
     /*display: table;*/
     margin-bottom: 20px;
 }


 .product-wrapper{

     display: table;
     width: 100%;
     margin-bottom: 50px;

 }

 .product-image-box{
     width: 50%;
     display:block;
     float:left;
     /*margin: 10px 10px 10px 0px;*/

 }

 .image-grid{

     display:block;
     float:left;
     /*width: 110px;*/
     overflow: hidden;
     margin-right: 10px;

 }


 .slides-grid{

     margin: 10px 5px;

 }


 .slick-slide{

     padding: 3px;
     /*height: 100px;*/
     overflow: hidden;
     margin-bottom: 10px;
     border: 1px solid #cccccc;
     /*border-color: rgb(231, 118, 0);
	box-shadow: rgba(228, 121, 17, 0.498039) 3px 3px 3px 3px;*/


 }

 .slick-slide:hover{

     border-color: #3399CC;
     box-shadow: rgba(51, 153, 204, 0.498039) 0px 0px 3px 3px;

 }

 .active{

     border-color: #3399CC;
     box-shadow: rgba(51, 153, 204, 0.498039) 0px 0px 3px 3px;

 }

 .product-image{

     height:400px;
     width:400px;
     overflow: hidden;
     outline-color: red;
     text-align: center;
     /*border: 1px solid red;*/

 }

 .product-image>img{
     height:400px;
     width:400px;
 }


 .product-shop{

     width: 50%;
     /*margin: 10px 10px 10px 0px;*/
     display:block;
     float:left;
     /*border: 1px solid lime;*/
     text-align: center;

 }

 .product-name>h1{

     color: #3399cc;
     font-size: 28px;
     font-weight: 400;
     padding-bottom: 3px;
     text-transform: uppercase;
     font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
     text-align: left;

 }

 .product-special-price-container{

     text-align: center;

     color: #3399cc;
     color: #9e9e9e;
     font-size: 24px;
     white-space: nowrap;
     font-family: "Helvetica Neue", Verdana, Arial, sans-serif;


     font-family: Georgia,"Times New Roman",Times,serif;
     font-style: italic;
     font-size: 32px;
     line-height: 1.1em;
     margin-bottom: 10px;
     display: inline-block;
     margin-right: 20px;
     text-decoration: line-through;
 }


 .product-price-container{

     text-align: center;

     color: #3399cc;
     color: #e09d00;
     font-size: 24px;
     white-space: nowrap;
     font-family: "Helvetica Neue", Verdana, Arial, sans-serif;
     font-family: Georgia,"Times New Roman",Times,serif;
     font-style: italic;
     font-size: 40px;
     line-height: 1.1em;
     margin-bottom: 20px;
     display: inline-block;
 }


 .service-information{

     font-family: "Helvetica Neue", Verdana, Arial, sans-serif;
     color: #636363;
     font-size: 12px;
     line-height: 1.5;
     margin-bottom: 20px;
     clear: both;
     padding-bottom: 20px;
     border-bottom: 1px solid #cccccc;
     /*outline: 1px solid green;*/


 }

 .service-information li{

     display: inline-block;
     border:1px solid #3399cc;
     padding: 3px;
     width: 31%;
     text-align: center;

 }

 .service-information li:hover{

     color: white;
     background-color: #3399cc;
     cursor: pointer;

 }

 #popup, .bMulti {
     margin-top: 5%;
     min-height: 250px;
     max-width: 500px;
     width: 100%;
 }

 #popup, #popup2, .bMulti {
     background-color: #fff;
     border-radius: 10px 10px 10px 10px;
     box-shadow: 0 0 25px 5px #999;
     color: #111;
     display: none;
     min-width: 550px;
     padding: 25px;
 }

 .button.b-close, .button.bClose {
     border-radius: 7px 7px 7px 7px;
     box-shadow: none;
     font: bold 131% sans-serif;
     padding: 0 6px 2px;
     position: absolute;
     /*
	right: -8px;
	top: -8px;
      */
 }

 .b-close {
     width: 24px;
     height: 24px;
     box-shadow: none;
     font: bold 131% sans-serif;
     padding: 3px;
     position: absolute;
     right: -14px;
     top: -14px;

     background-color: #3399cc;
     border-radius: 30px;
     box-shadow: 0 2px 3px rgba(0,0,0,0.3);
     color: #fff;
     cursor: pointer;
     display: inline-block;
     text-align: center;
     text-decoration: none;
     border:2px solid white;
     vertical-align: middle;


 }

 .b-close:hover{

     background: #247096;

 }

 .b-close>span {
     font-size: 85%;
 }


 #popup, #popup2, .bMulti {
     background-color: #fff;
     border-radius: 10px 10px 10px 10px;
     box-shadow: 0 0 30px 3px #999;
     color: #111;
     display: none;
     min-width: 450px;
     padding: 25px;
 }



 .std{

     text-align: left;

 }

 .add-to-cart{

     text-align: left;

 }


 .qty-wrapper{

     display: inline-block;
     /*width: 100px;*/
     margin: 0 10px;

 }

 .qty-wrapper>label{

     font-size: 13px;
     font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
     font-weight: 600;
     color: #636363;

 }



 #qty{

     width: 35px;
     height: 40px;
     border-radius: 2px;
     border: 1px solid silver;
     background: #FFFFFF;
     font-size: 15px;
     text-align: center;
     vertical-align: middle;

 }

 .btn-cart{



     background: #3399cc;
     display: inline-block;
     padding: 7px 15px;
     border: 0;
     color: #FFFFFF;
     font-size: 13px;
     font-weight: normal;
     font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
     line-height: 19px;
     text-align: center;
     text-transform: uppercase;
     vertical-align: middle;
     white-space: nowrap;
     cursor: pointer;


     height: 40px;
     line-height: 40px;
     font-size: 16px;
     padding: 0px 30px;
     /*float: left;*/
     min-width: 160px;
     /*width: 100%;*/


 }

 .btn-cart:hover{

     background: #247096;

 }

 .tabs{
     cursor:pointer;
     float: left;
     /*margin-right: 5px;*/
     display: block;
     padding: 0px 40px;
     /*background-color: #ecf0f1;*/
     border: solid 1px #cccccc;

     /*border-radius:20px 20px 0 0;*/

     height: 40px;
     line-height: 40px;
     text-transform: uppercase;
     font-size: 14px;
     /*font-family: "Helvetica Neue", Verdana, Arial, sans-serif;*/
     font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
     margin-right: -1px;
     margin-bottom: -1px;
     background-color: #f4f4f4;
     color: #636363;

 }


 .description-tabs>input {
     display: none;
 }

 &:checked + label {

     background-color: #e74c3c;
     color:#fefefe;


 }

 input[id="tab-1"]:checked ~ .tab1{

     color:#3399cc;
     background-color: white;
 }

 input[id="tab-2"]:checked ~ .tab2{

     color:#3399cc;
     background-color: white;

 }
 input[id="tab-3"]:checked ~ .tab3{

     color:#3399cc;
     background-color: white;

 }
 input[id="tab-4"]:checked ~ .tab4{

     color:#3399cc;
     background-color: white;

 }

 input[id="tab-1"]:checked ~ .content .tab-1,
 input[id="tab-2"]:checked ~ .content .tab-2,
 input[id="tab-3"]:checked ~ .content .tab-3,
 input[id="tab-4"]:checked ~ .content .tab-4{
     display: block;
     border:solid 1px #cccccc;
     /*border-top:1px solid white;*/
     margin-top:-1px;
     font-size: 14px;
     font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
     //font-weight: 600;
     color: #636363;
 }

 .content {
     clear:both;
     position: relative;
 }

 .content>article {

     width: 100%;
     padding: 20px 20px;
     /*position: absolute;*/
     top: 0;
     left: 0;
     //text-indent: 30px;
     display: none;
     /*border-top:2px solid black;*/
     /*border-radius:0 10px 10px 10px;*/
     font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
     color: #636363;
     font-size: 16px;
     line-height:24px;
     max-width: 1200px;
     -moz-box-sizing: border-box; /* Для Firefox */
     box-sizing: border-box;


 }

 .content>article p{

     text-indent:30px;

 }




 .content>article ul{

     padding: 0 0 0 50px;

 }
 .content>article li{

     list-style-type: disc;

 }

 h2{

     margin: 10px 0;
     color: #636363;
     font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
     font-size: 20px;
     font-weight: 400;
     line-height: 24px;
     text-rendering: optimizeLegibility;
     text-transform: uppercase;
 }


 h2.subtitle {
     margin: auto;
     padding: 6px 0;
     text-align: center;
     color: #3399cc;
     font-weight: 400;
     border-bottom: 1px solid #cccccc;
     border-top: 1px solid #cccccc;
     cursor: pointer;
     max-width:1200px;
 }



 .products-grid{

     text-align: center;
     max-width: 1200px;
     margin: 20px auto ;

 }


 .item{

     display: inline-block;
     width: 20%;
     margin-right: 2.22222%;
     border: 1px solid #ededed;
     padding: 10px;
     margin: 10px 15px;
     margin-bottom: 30px;
     height: 350px;

 }

 .item:hover{

     border: 1px solid #3399cc;

 }

 a>.product-image{
     display: block;
     border: solid 1px #ededed;
     width: 100%;
     margin-bottom: 10px;


 }

 .product-image>img{

     max-width: 100%;

 }

 .product-image:hover{

     /*border: 1px solid #3399cc;*/

 }

 .product-name>a{

     text-transform: uppercase;
     margin-bottom: 5px;
     font-size: 14px;
     font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
     text-rendering: optimizeLegibility;
     color: #636363;
     font-weight: 500;

 }

 .product-name>a:hover{

     color:#3399cc;

 }

 .price{

     white-space: nowrap;
     font-family: Georgia,"Times New Roman",Times,serif;
     color: #e09d00;
     //font-size: 16px;
     font-style: italic;

 }

 #product-attribute-specs-table {
     width: 100%;
     max-width: 50em;
 }


 #product-attribute-specs-table th {
     border: 1px solid silver;

 }

 .data-table th {
     background: #f4f4f4;
     text-transform: uppercase;
     font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;
     line-height: 1.4;
     white-space: nowrap;
 }
 .data-table td, .data-table th {
     padding: 10px;
     vertical-align: top;
 }
 th, code, cite, caption {
     font-weight: normal;
     font-style: normal;
     text-align: left;
 }

 .data-table tbody td, .data-table tfoot td {
     font-family: Georgia, Times, "Times New Roman", serif;
 }
 .data-table thead th, .data-table tbody td {
     border: 1px solid silver;
     width: 100%;
 }
 .data-table td, .data-table th {
     padding: 10px;
     vertical-align: top;
 }

 #super-product-table{

     border: 1px solid silver;

 }

 .wrapper-components{

     border:1px solid silver;

 }

 .components{

     margin:30px;
     width: 70%;
     box-sizing: border-box;

 }

 .components{

     padding: 10px;
     text-align: left;

 }

 .components>tbody>tr{

     border-bottom:1px solid silver;
 }

 .components>tbody>.last{
     border-bottom:none;
 }

 tr>.imag{

     padding:4px;

 }

 .num{

     padding: 4px 20px;

 }

 .name-wrapper{

     /*color: #3399cc;*/
     font-family: "Raleway", "Helvetica Neue", Verdana, Arial, sans-serif;

 }

 .name-wrapper>a{

     color: #3399cc;

 }


 .item>a>img{

     height:240px;
     width:240px;

 }


 h3.product-name{

     height:60px;
     margin-bottom:20px;
     overflow:hidden;

 }


 .price-box{

     font-size:26px;

 }




</style>




















<script src="/../../../lib/jquery.bpopup.min.js"></script>

<script type="text/javascript">

 var $ = jQuery.noConflict();

 $(document).ready(function() {

     $( ".slick-slide" ).on( "mouseover", function() {
 	 var el = $(this);
 	 var img = $(el).children().attr('src');
 	 var screen = $('.gallery-image').attr('src', img);

 	 var cls =  $($(this)[0]).attr('class');

 	 if (cls.indexOf('active') === -1){

 	     $('.active').attr('class','slick-slide');
 	     $(this).attr('class','slick-slide active');

 	 }



     });

 });

</script>
<script type="text/javascript">

 $(document).ready(function(){



     popup = function(param){

	 $('.'+param+'-inf').on('click',function(){
	     $('.popup-'+param).bPopup({
		 modalClose: true,
		 opacity: 0.82,
		 speed: 650,
	         transition: 'slideIn',
		 transitionClose: 'slideBack'
	     });
	 })

     }


     popup('delivery');
     popup('timetable');
     popup('contact');


     $('.btn-cart').on('click',function(){

	 frm = $('#frm').serialize()

	 $.ajax({
	     type: "POST",
	     url: "/checkout/cart",
	     data: frm,

	     success: function(msg){
		 /*$("body").append(msg);*/
		 console.log($('#frm').serialize());
	     }
	 });



	 function getSku(){
	     var frm = $('#frm').serialize();

	     return (frm.substring(frm.indexOf('=')+1,frm.indexOf('&')));
	 }

	 function getQty(){
	     var frm = $('#frm').serialize();
	     return (frm.substring(frm.indexOf('&')+5));
	 }

	 function addToCart(){

	     var cart = getCart();

	     if(cart){

		 var sku = getSku();
		 var qty = getQty();

		 cart[sku] = qty;
		 setCart(cart);


	     }else{

		 cartInit();
		 addToCart();
	     }

	 }


	 function getCart(){

	     var cart;
	     cart = getCookie('cart');
	     if(cart){

		 cart = decodeCart(cart);
		 cart = unserialize(cart);
		 return cart;

	     }else{

		 return false;
	     }


	 }

	 function setCart(cart){

	     options = {expires:3600*24*7,path:"/"}
	     cart = serialize(cart);
	     cart = encodeCart(cart);
	     setCookie('cart', cart, options);


	 }

	 function cartInit(){

	     var cart=[];

	     var rand = Math.floor(Math.random()* 10000000);
	     cart['cartid'] = rand;
	     setCart(cart);
	     return cart;

	 }


	 function getCookie(name) {

	     var matches = document.cookie.match(new RegExp(
		 "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	     ));

	     return matches ? decodeURIComponent(matches[1]) : undefined;
	 }

	 function setCookie(name, value, options) {
	     options = options || {};

	     var expires = options.expires;

	     if (typeof expires == "number" && expires) {
		 var d = new Date();
		 d.setTime(d.getTime() + expires * 1000);
		 expires = options.expires = d;
	     }
	     if (expires && expires.toUTCString) {
		 options.expires = expires.toUTCString();
	     }

	     value = encodeURIComponent(value);

	     var updatedCookie = name + "=" + value;

	     for (var propName in options) {
		 updatedCookie += "; " + propName;
		 var propValue = options[propName];
		 if (propValue !== true) {
		     updatedCookie += "=" + propValue;
		 }
	     }

	     document.cookie = updatedCookie;
	 }


	 function decodeCart(data){


	     return window.atob(data);

	 }

	 function encodeCart(data){

	     return window.btoa(data);

	 }

	 function serialize (mixedValue) {
	     //  discuss at: http://locutus.io/php/serialize/
	     // original by: Arpad Ray (mailto:arpad@php.net)
	     // improved by: Dino
	     // improved by: Le Torbi (http://www.letorbi.de/)
	     // improved by: Kevin van Zonneveld (http://kvz.io/)
	     // bugfixed by: Andrej Pavlovic
	     // bugfixed by: Garagoth
	     // bugfixed by: Russell Walker (http://www.nbill.co.uk/)
	     // bugfixed by: Jamie Beck (http://www.terabit.ca/)
	     // bugfixed by: Kevin van Zonneveld (http://kvz.io/)
	     // bugfixed by: Ben (http://benblume.co.uk/)
	     // bugfixed by: Codestar (http://codestarlive.com/)
	     //    input by: DtTvB (http://dt.in.th/2008-09-16.string-length-in-bytes.html)
	     //    input by: Martin (http://www.erlenwiese.de/)
	     //      note 1: We feel the main purpose of this function should be to ease
	     //      note 1: the transport of data between php & js
	     //      note 1: Aiming for PHP-compatibility, we have to translate objects to arrays
	     //   example 1: serialize(['Kevin', 'van', 'Zonneveld'])
	     //   returns 1: 'a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}'
	     //   example 2: serialize({firstName: 'Kevin', midName: 'van'})
	     //   returns 2: 'a:2:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";}'

	     var val, key, okey
	     var ktype = ''
	     var vals = ''
	     var count = 0

	     var _utf8Size = function (str) {
		 var size = 0
		 var i = 0
		 var l = str.length
		 var code = ''
		 for (i = 0; i < l; i++) {
		     code = str.charCodeAt(i)
		     if (code < 0x0080) {
			 size += 1
		     } else if (code < 0x0800) {
			 size += 2
		     } else {
			 size += 3
		     }
		 }
		 return size
	     }

	     var _getType = function (inp) {
		 var match
		 var key
		 var cons
		 var types
		 var type = typeof inp

		 if (type === 'object' && !inp) {
		     return 'null'
		 }

		 if (type === 'object') {
		     if (!inp.constructor) {
			 return 'object'
		     }
		     cons = inp.constructor.toString()
		     match = cons.match(/(\w+)\(/)
		     if (match) {
			 cons = match[1].toLowerCase()
		     }
		     types = ['boolean', 'number', 'string', 'array']
		     for (key in types) {
			 if (cons === types[key]) {
			     type = types[key]
			     break
			 }
		     }
		 }
		 return type
	     }

	     var type = _getType(mixedValue)

	     switch (type) {
		 case 'function':
		     val = ''
		     break
		 case 'boolean':
		     val = 'b:' + (mixedValue ? '1' : '0')
		     break
		 case 'number':
		     val = (Math.round(mixedValue) === mixedValue ? 'i' : 'd') + ':' + mixedValue
		     break
		 case 'string':
		     val = 's:' + _utf8Size(mixedValue) + ':"' + mixedValue + '"'
		     break
		 case 'array':
		 case 'object':
		     val = 'a'
		     /*
			if (type === 'object') {
			var objname = mixedValue.constructor.toString().match(/(\w+)\(\)/);
			if (objname === undefined) {
			return;
			}
			objname[1] = serialize(objname[1]);
			val = 'O' + objname[1].substring(1, objname[1].length - 1);
			}
		      */

		     for (key in mixedValue) {
			 if (mixedValue.hasOwnProperty(key)) {
			     ktype = _getType(mixedValue[key])
			     if (ktype === 'function') {
			         continue
			     }

			     okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key)
			     vals += serialize(okey) + serialize(mixedValue[key])
			     count++
			 }
		     }
		     val += ':' + count + ':{' + vals + '}'
		     break
		 case 'undefined':
		 default:
		     // Fall-through
		     // if the JS object has a property which contains a null value,
		 // the string cannot be unserialized by PHP
		     val = 'N'
		     break
	     }
	     if (type !== 'object' && type !== 'array') {
		 val += ';'
	     }

	     return val
	 }


	 function unserialize (data) {
	     //  discuss at: http://locutus.io/php/unserialize/
	     // original by: Arpad Ray (mailto:arpad@php.net)
	     // improved by: Pedro Tainha (http://www.pedrotainha.com)
	     // improved by: Kevin van Zonneveld (http://kvz.io)
	     // improved by: Kevin van Zonneveld (http://kvz.io)
	     // improved by: Chris
	     // improved by: James
	     // improved by: Le Torbi
	     // improved by: Eli Skeggs
	     // bugfixed by: dptr1988
	     // bugfixed by: Kevin van Zonneveld (http://kvz.io)
	     // bugfixed by: Brett Zamir (http://brett-zamir.me)
	     //  revised by: d3x
	     //    input by: Brett Zamir (http://brett-zamir.me)
	     //    input by: Martin (http://www.erlenwiese.de/)
	     //    input by: kilops
	     //    input by: Jaroslaw Czarniak
	     //      note 1: We feel the main purpose of this function should be
	     //      note 1: to ease the transport of data between php & js
	     //      note 1: Aiming for PHP-compatibility, we have to translate objects to arrays
	     //   example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}')
	     //   returns 1: ['Kevin', 'van', 'Zonneveld']
	     //   example 2: unserialize('a:2:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";}')
	     //   returns 2: {firstName: 'Kevin', midName: 'van'}

	     var $global = (typeof window !== 'undefined' ? window : GLOBAL)

	     var utf8Overhead = function (chr) {
		 // http://locutus.io/php/unserialize:571#comment_95906
		 var code = chr.charCodeAt(0)
		 var zeroCodes = [
		     338,
		     339,
		     352,
		     353,
		     376,
		     402,
		     8211,
		     8212,
		     8216,
		     8217,
		     8218,
		     8220,
		     8221,
		     8222,
		     8224,
		     8225,
		     8226,
		     8230,
		     8240,
		     8364,
		     8482
		 ]
		 if (code < 0x0080 || code >= 0x00A0 && code <= 0x00FF || zeroCodes.indexOf(code) !== -1) {
		     return 0
		 }
		 if (code < 0x0800) {
		     return 1
		 }
		 return 2
	     }
	     var error = function (type,
				   msg, filename, line) {
		 throw new $global[type](msg, filename, line)
	     }
	     var readUntil = function (data, offset, stopchr) {
		 var i = 2
		 var buf = []
		 var chr = data.slice(offset, offset + 1)

		 while (chr !== stopchr) {
		     if ((i + offset) > data.length) {
			 error('Error', 'Invalid')
		     }
		     buf.push(chr)
		     chr = data.slice(offset + (i - 1), offset + i)
		     i += 1
		 }
		 return [buf.length, buf.join('')]
	     }
	     var readChrs = function (data, offset, length) {
		 var i, chr, buf

		 buf = []
		 for (i = 0; i < length; i++) {
		     chr = data.slice(offset + (i - 1), offset + i)
		     buf.push(chr)
		     length -= utf8Overhead(chr)
		 }
		 return [buf.length, buf.join('')]
	     }
	     var _unserialize = function (data, offset) {
		 var dtype
		 var dataoffset
		 var keyandchrs
		 var keys
		 var contig
		 var length
		 var array
		 var readdata
		 var readData
		 var ccount
		 var stringlength
		 var i
		 var key
		 var kprops
		 var kchrs
		 var vprops
		 var vchrs
		 var value
		 var chrs = 0
		 var typeconvert = function (x) {
		     return x
		 }

		 if (!offset) {
		     offset = 0
		 }
		 dtype = (data.slice(offset, offset + 1)).toLowerCase()

		 dataoffset = offset + 2

		 switch (dtype) {
		     case 'i':
			 typeconvert = function (x) {
			     return parseInt(x, 10)
			 }
			 readData = readUntil(data, dataoffset, ';')
			 chrs = readData[0]
			 readdata = readData[1]
			 dataoffset += chrs + 1
			 break
		     case 'b':
			 typeconvert = function (x) {
			     return parseInt(x, 10) !== 0
			 }
			 readData = readUntil(data, dataoffset, ';')
			 chrs = readData[0]
			 readdata = readData[1]
			 dataoffset += chrs + 1
			 break
		     case 'd':
			 typeconvert = function (x) {
			     return parseFloat(x)
			 }
			 readData = readUntil(data, dataoffset, ';')
			 chrs = readData[0]
			 readdata = readData[1]
			 dataoffset += chrs + 1
			 break
		     case 'n':
			 readdata = null
			 break
		     case 's':
			 ccount = readUntil(data, dataoffset, ':')
			 chrs = ccount[0]
			 stringlength = ccount[1]
			 dataoffset += chrs + 2

			 readData = readChrs(data, dataoffset + 1, parseInt(stringlength, 10))
			 chrs = readData[0]
			 readdata = readData[1]
			 dataoffset += chrs + 2
			 if (chrs !== parseInt(stringlength, 10) && chrs !== readdata.length) {
			     error('SyntaxError', 'String length mismatch')
			 }
			 break
		     case 'a':
			 readdata = {}

			 keyandchrs = readUntil(data, dataoffset, ':')
			 chrs = keyandchrs[0]
			 keys = keyandchrs[1]
			 dataoffset += chrs + 2

			 length = parseInt(keys, 10)
			 contig = true

			 for (i = 0; i < length; i++) {
			     kprops = _unserialize(data, dataoffset)
			     kchrs = kprops[1]
			     key = kprops[2]
			     dataoffset += kchrs

			     vprops = _unserialize(data, dataoffset)
			     vchrs = vprops[1]
			     value = vprops[2]
			     dataoffset += vchrs

			     if (key !== i) {
			         contig = false
			     }

			     readdata[key] = value
			 }

			 if (contig) {
			     array = new Array(length)
			     for (i = 0; i < length; i++) {
			         array[i] = readdata[i]
			     }
			     readdata = array
			 }

			 dataoffset += 1
			 break
		     default:
			 error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype)
			 break
		 }
		 return [dtype, dataoffset - offset, typeconvert(readdata)]
	     }

	     return _unserialize((data + ''), 0)[2]
	 }



	 addToCart();
	 document.location = '/checkout/cart';
     })



 })

</script>





<div class="breadcrumbs">
    <ul>
        <li class="home">
            <a href="http://magento-demo.lexiconn.com/" title="Go to Home Page">Home</a>
            <span>/ </span>
        </li>
        <li class="category4">
            <strong>legrand</strong>
        </li>
    </ul>
</div>







<div id="popup" class="popup-delivery" style="">
    <span class="b-close"><span>X</span></span>

    <h2>Способы доставки:</h2>
    <ul>
	<li>Самовывоз</li>
	<li>Транспортная компания<br>
	    <p>Доставка во все регионы Украины осуществляется ведущими перевозчиками: Новая Почта, Ин-Тайм, Деливери, Ночной экспресс, САТ, Автолюкс, и другими по желанию покупателя.</p>
	</li>

	<li>Нова Пошта</li>
    </ul>

    <h2>Способы оплаты:</h2>
    <ul>
	<li>Наличными
	    <p>Оплата наличными при получении товара. Оплатить заказ наличными деньгами Вы можете только в нашем офисе.</p>
	</li>
	<li>Наложенный платеж<br>
	    <p>Для отправки продукции наложенным платежем, необходимо осуществить частичную предоплату, в размере не менее 100 грн.</p>
	</li>

	<li>Перевод на карту Приватбанка

	    <p>Нет комисси при оплате через Приват 24 или Liqpay. Комиссия 2 грн. при переводе через терминал. Доставка товара возможна только после подтверждения платежа.</p>

	</li>
    </ul>
    <h2>Регион доставки:</h2>
    <ul>Украина</ul>
</div>


<div id="popup" class="popup-timetable" style="">
    <span class="b-close"><span>X</span></span>

</div>

<div id="popup" class="popup-contact" style="">
    <span class="b-close"><span>X</span></span>

</div>




















<div class="col-main">



    <div class="product-wrapper">

	<div class="product-image-box">

	    <div class="image-grid">
		<ul class="slides-grid">

		    <li class="slick-slide active">
			<img width="50" height="50" src="<?php echo "../../".$data['imgAdres']?>">
		    </li>

		    <?php
		    foreach ($data as $key => $value) {

			if(stripos($key,"img") !== false && $key !== "imgAdres" ){


		    ?>

			<li class="slick-slide">
			    <img width="50" height="50" src="<?php echo "../../".$value ?>">
			</li>

		    <?php }} ?>



		</ul>
	    </div>

	    <div class = "product-image">
		<img id="image-main" class="gallery-image visible" src="<?php echo "../../".$data['imgAdres'] ?>" alt="Фотография готовится" title="Shea Enfused Hydrating Body Lotion">
	    </div>

	</div>

	<div class = "product-shop">

	    <div class="product-name">
		<h1><?php echo $data['name'] ?></h1>
	    </div>


	    <?php


	    if(!(float)$data['specialPrice']){
		$css_price = 'product-price-container';
		$css_sprice = 'product-special-price-container';
	    }else{
		$css_sprice = 'product-price-container';
		$css_price = 'product-special-price-container';
	    }

	    ?>

	    <div class="<?php echo $css_price ?>">
		<span class="product-price"><?php echo $data['price'] ." грн"?></span>
	    </div>



	    <div class="<?php echo $css_sprice ?>">
		<span class="product-price"><?php if((float)$data['specialPrice']) echo $data['specialPrice'] ." грн"?> </span>
	    </div>


	    <div class="service-information">

		<ul class="information-block">

		    <li class="delivery-inf">Условия оплаты и доставки</li>
		    <li class="timetable-inf">График работы</li>
		    <li class="contact-inf">Адрес и контакты</li>

		</ul>

	    </div>



	    <!--
	         <div class="std">Выключатель и розетка – сегодня важная функциональная деталь в каждом доме, без которой трудно представить себе любое помещение. При выборе розетки важным фактором является не только качество, но и дизайн, ведь она должна гармонично вписаться в общий стиль интерьера.
	         <p>Создатели серии Legrand Valena преследовали именно эту цель – сделать розетки удобными, многофункциональными, долговечными и красивыми.</p>

	         </div>
	         </div>-->
	    <form id="frm" name="prod" enctype="multipart/form-data" action="/checkout/cart" method="POST">
	        <div class="add-to-cart">
		    <div class="qty-wrapper">
			<label for="qty">Кол-во:</label>

			<input type=hidden name=sku value="<?php echo $data['sku'] ?>">
			<input type="text" name="qty" id="qty" maxlength="12" value="1" title="Qty" class="input-text qty">

		    </div>

		    <button type="submit"  class="button btn-cart" onclick="">
			<span>
			    <span>Добавить в корзину</span>
			</span>
		    </button>
		</div>
	    </form>
	</div>
    </div>









    <div class="description-tabs">
	<input type="radio" name="tab" id="tab-1" checked>
	<label class = "tabs tab1" for="tab-1">Описание</label>
	<input type="radio" name="tab" id="tab-2">
	<label class = "tabs tab2" for="tab-2">Характеристики</label>
	<input type="radio" name="tab" id="tab-3">
	<label class = "tabs tab3" for="tab-3" <?php if (!$subprod) echo'style="display:none;"'?>">Компоненты товара</label>
	<!--
	     <input type="radio" name="tab" id="tab-4">
	     <label class = "tabs tab4" for="tab-4">Отзывы</label>
	-->
	<div class="content">
	    <article class="tab-1">
		<?php echo $data['description']  ?>
	    </article>
	    <article class="tab-2">

		<table class="data-table" id="product-attribute-specs-table">
		    <colgroup><col width="25%">
			<col>
		    </colgroup>

		    <tbody>

			<tr class="first odd">
			    <th class="label">Артикул</th>
			    <td class="data last"><?php echo $data['sku']  ?></td>
			</tr>


			<tr class="even">
			    <th class="label">Бренд</th>
			    <td class="data last"><?php echo $data['brand']  ?></td>
			</tr>

		    </tbody>
    		</table>

	    </article>




	    <article class="tab-3">


		<div class="wrapper-components">

		    <table class="components">

			<?php foreach ($subprod as $key => $value) { ?>

			    <tr>
				<td class="imag">
				    <a href="<?php echo "http://smart-electric.com.ua/product".$value[urlAdres]."/sku/".$value[sku]?>">
					<img class="thumbnail" width="100" height="100" src= "<?php echo "/".$value[imgAdres] ?>" alt="<?php echo $value[name]?>" title="<?php echo $value[name]?>">
				    </a>

				</td>
				<td class="num">
				    <p class="name-wrapper">

                        		<?php echo $value[qty] ." шт."  ?>

                        	    </p>
                        	</td>
                        	<td>
                        	    <p class="name-wrapper">
                        		<a href="">

					    <?php echo $value[name]?>
                        		</a>
                        	    </p>
				</td>
				<td>
				    <span class="regular-price" id="product-price-388">
                                    	<span class="price"><?php echo $value[price] ." грн."  ?></span>
                                    </span>
				</td>
			    </tr>

			<?php } ?>
		    </table>

		</div>


	    </article>

	    <article class="tab-4">
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore numquam, itaque laboriosam debitis temporibus incidunt, dicta dignissimos sapiente voluptatibus iusto. Voluptate, laborum libero odio nostrum consequuntur aliquid molestiae soluta optio.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore numquam, itaque laboriosam debitis temporibus incidunt, dicta dignissimos sapiente voluptatibus iusto. Voluptate, laborum libero odio nostrum consequuntur aliquid molestiae soluta optio.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	    </article>
	</div>
	<br class="clear:both" />
    </div><!-- //wrapper -->

</div>


<h2 class="subtitle">Вам также понравится</h2>




























<ul class="products-grid">

    <?php

    foreach ($crossProd as $value){

	if(mb_strlen($value['shortName'])>50){
	    $value['shortName'] = mb_substr($value['shortName'], 0,48);
	    $value['shortName'] = $value['shortName']." .......";
	}


    ?>

    <!--
	 <li class="item last">
	 <a href="<?php echo "../".$value['urlAdres']."/"."sku/".$value['sku'] ?>" title="<?php echo $value['shortName']?>" class="product-image">
	 <img src="<?php echo '../../'.$value['imgAdres'] ?>" alt="<?php echo $value['Name'] ?>">
	 </a>

	 <h3 class="product-name"><a href="http://magento-demo.lexiconn.com/elizabeth-knit-top-592.html" title="<?php echo $value['Name'] ?>">
	 <?php echo $value['shortName']   ?></a></h3>

	 <div class="price-box">
	 <span class="regular-price" id="product-price-421-new">
	 <span class="price"><?php echo $value['price']?></span>
	 </span>

	 </div>

	 </li>

    -->


    <li class="item last">
	<a href="<?php echo "/"."product"."/"."sku/".$value['sku'] ?>" title="<?php echo $value['name']?>" class="product-image">
	    <img src="<?php echo '../../'.$value['imgAdres'] ?>" alt="<?php echo $value['name'] ?>">
	</a>

	<h3 class="product-name"><a href="<?php echo "/"."product"."/"."sku/".$value['sku'] ?>">
	    <?php echo $value['shortName']?></a></h3>

	<div class="price-box">

	    <?php

	    if(!(float)$value['specialPrice']){

		$css_price = "regular-price";
		$css_sprice = "special-price";
	    }else{

		$css_sprice = "regular-price";
		$css_price = "special-price";
	    }



	    ?>

	    <span class="<?php echo $css_sprice ?>" id="product-price-421-new">
	        <span class="price"><?php echo $value['price'] . " грн "?></span>
	    </span>

	    <span class="<?php echo $css_price ?>" id="product-price-421-new">
	        <span class="price"><?php if((float) $value['specialPrice']) echo $value['specialPrice'] . " грн";?></span>
	    </span>

	</div>

    </li>

	    <?php }?>


            <!--
		 <div class="actions">
                 <button type="button" title="Add to Cart" class="button btn-cart" onclick="setLocation('http://magento-demo.lexiconn.com/elizabeth-knit-top-592.html')">
                 <span>
                 <span>Add to Cart</span>
                 </span>
                 </button>
		 </div>
            -->
</ul>
