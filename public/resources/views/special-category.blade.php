<section class = "special-category-block">
  <h2 class="special-category-head">Специальные дизайнерские решения</h2>

  <ul class = "special-category-list">

    <li class="special-1">
      <a href="/product/1" class="special-category-link">
	<div class="special-category-img">
	  <img src="images/sp_1.webp">
	</div>
	<div class="special-category-description">
	  <h3>Turn heads</h3>
	  <span class="dotted"></span>
	  <p>
	 Our accent chairs are scene stealers – bold shapes, bold styles, bold colours. Add flare to your favourite corner.
	 </p>
	</div>
      </a>
    </li>

    <li class="special-1">
      <a href="/product/2" class="special-category-link">
	<div class="special-category-description">
	  <h3>PUT IT DOWN</h3>
	  <span class="dotted"></span>
	  <p>
	    What looks better on top of a rug? Everything. Try it.
	 </p>
	</div>
	<div class="special-category-img">
	  <img src="images/sp_2.webp">
	</div>
      </a>
    </li>










    
    <li class="special-last">

      <a href="/" class="special-category-link">
	<div class="special-category-description">
	  <img src="images/sp_3.webp">

	  <h3>NEW: TalentLAB</h3>
	  <span class="dotted"></span>
	  <p>
	    We've launched our crowdsourcing, crowdfunding platform. Find new designers first. Or submit your own idea for the chance to get it MADE.
	  </p>
	</div>
      </a>

      <a href="/" class="special-category-link">
	<div class="special-category-description">
	  <img src="images/sp_3.webp">
	  <h3>Turn heads</h3>
	  <span class="dotted"></span>
	  <p>Our accent chairs are scene stealers – bold shapes, bold styles, bold colours. Add flare to your favourite corner.
	 </p>
	</div>
      </a>

    </li>

  </ul>

</section>
