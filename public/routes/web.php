<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
Route::get('/start', function () {
    return view('welcome');
});
*/



Route::get('/', function () {
    return view('home/home');
});


Route::get('/welcome', function () {
    return view('links/welcome');
});


Route::get('/pay', function () {
    return view('links/pay');
});


Route::get('/contacts', function () {
    return view('links/contacts');
});


Route::get('/delivery', function () {
    return view('links/delivery');
});


Route::get('/about', function () {
    return view('links/about');
});





//Route::model('product', 'Product');
//Route::get('/product', 'ProductController@index');



Route::get('/product/{id}', 'ProductController@index')->where('id', '.*')->name('product');
Route::get('/category/{id}', 'CategoryController@index')->where('id', '.*')->name('category');


Route::any('/{search}', 'SearchController@index')->where('search', 'search*')->name('search');


//Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');


Route::any('/logout', 'LogoutController@index')->name('logout');



Auth::routes();
Route::get('/admin', 'AdminController@index')->name('admin');

Auth::routes();
Route::get('/admin/{param}', 'AdminController@index')->where('param', '.*');



Route::get('image-upload', 'ImageUploadController@imageUpload')->name('image.upload');
Route::post('image-upload', 'ImageUploadController@imageUploadPost')->name('image.upload.post');



//Route::post('/api/product', 'ProductController@index');

/*
Route::match(['get', 'post'], '/', function () {
   //
});
*/


Route::any('/api/product/save', 'Api\ProductController@index')->name('api.admin.product.save');
Route::any('/api/product/edit', 'Api\ProductController@edit')->name('api.admin.product.edit');
Route::any('/api/product/delete', 'Api\ProductController@destroy')->name('api.admin.product.delete');


Route::any('/api/products', 'Api\ProductController@show')->name('api.admin.products');

Route::any('/api/product/showProduct', 'Api\ProductController@showProduct')->name('api.admin.product.show');


Route::any('/api/product/getImages', 'Api\ProductController@images')->name('api.admin.product.images');
Route::any('/api/product/getImageDefault', 'Api\ProductController@imageDefault')->name('api.admin.product.image.default');


Route::any('/api/product/subproduts', 'Api\ProductController@getSubproduts')->name('api.admin.product.subproduct');
Route::any('/api/product/{id}', 'Api\ProductController@getComponents')->where('id', '.*')->name('api.admin.product.components');


Route::any('/api/categories', 'Api\CategoryController@show')->name('api.admin.categories');

Route::any('/api/category/showCategory', 'Api\CategoryController@showCategory')->name('api.admin.category.show');
Route::any('/api/category/save', 'Api\CategoryController@save')->name('api.admin.category.save');
Route::any('/api/category/edit', 'Api\CategoryController@edit')->name('api.admin.category.edit');
Route::any('/api/category/delete', 'Api\CategoryController@destroy')->name('api.admin.category.delete');



Route::any('/api/catalogs', 'Api\CatalogController@showCatalogs')->name('api.admin.catalog.show');

Route::any('/api/catalog/show', 'Api\CatalogController@show')->name('api.admin.catalog.show');
Route::any('/api/catalog/save', 'Api\CatalogController@save')->name('api.admin.catalog.save');
Route::any('/api/catalog/edit', 'Api\CatalogController@edit')->name('api.admin.catalog.edit');
Route::any('/api/catalog/delete', 'Api\CatalogController@delete')->name('api.admin.catalog.delete');







Route::any('/api/orders', 'Api\OrderController@showOrders')->name('api.admin.orders.show');
Route::any('/api/order/showOrder', 'Api\OrderController@showOrder')->name('api.admin.orders.show.order');
Route::any('/api/order/delete', 'Api\OrderController@delete')->name('api.admin.order.delete');



Route::any('/api/order/show', 'Api\OrderController@show')->name('api.admin.order.show');






Route::any('/checkout/cart', 'CartController@index')->name('checkout.cart');
Route::any('/checkout/addToCart', 'CartController@addToCart')->name('checkout.cart.add');

Route::any('/checkout/change', 'CartController@changeCart')->name('checkout.cart.change');
Route::any('/checkout/delete', 'CartController@delCart')->name('checkout.cart.delete');




Route::any('/checkout/final', 'CartController@final')->name('checkout.final');



Route::any('/checkout/thank_you', 'CartController@lastPage')->name('checkout.lastPage');
